import pandas as pd
import numpy as np
from datetime import datetime

from utils.candlestick import BinancePricesRepository
from utils.pair_strategies import Holding

class SimpleTrendClasifier():

    def linreg(self, timeserie):
        y = (timeserie.pct_change().cumsum() + 1).reset_index(drop=True).replace(np.nan, 1).rename('y')
        
        x = pd.Series(range(0,len(y)), name='x')
        x_ = x.mean()
        y_ = y.mean()
        x_dev = x.std()
        y_dev = y.std()
        
        corr = pd.DataFrame([x,y]).T.corr().iloc[0,1]
        slope = corr * (y_dev/x_dev)
        inter = y_ - slope*x_
        reg = x * slope + inter

        return slope

    def classify_trend(self, timeserie, drift_threshold=0.0015):
        slope = self.linreg(timeserie)
        
        if abs(slope) < drift_threshold:
            trend = 0
        else:
            trend = 1 if slope > drift_threshold else -1
                    
        return trend


class TrendFollowerStrategy(Holding):

    CANDLES_NEEDED_FOR_RUNNING = 24 * 5
    USE_SIGNALS = True
    LONG = 1
    CLOSE_LONG = 0

    def __init__(self, 
        base_currency: str, 
        quote_currency: str,
        n_for_tendency: int = 18,
        threshold_factor: float = 1.03,
        low_threshold_factor: float = 1.01,
        time_frame: int = 60,
        use_signals: int = 0,
        trend_classifier=SimpleTrendClasifier(),
        prices_repository=BinancePricesRepository()):

        super().__init__(base_currency, quote_currency, time_frame, prices_repository)         

        self.n_for_tendency = n_for_tendency
        self.threshold_factor = threshold_factor
        self.low_threshold_factor = low_threshold_factor
        self.use_signals = use_signals
        self.trend_classifier = trend_classifier

    def run(self, last_recommendation:float = 0):
        candles = self.get_candles_data()
        timestamp = candles.index[-1]
        price_variation = self.calculate_prices_variation(candles['open'])

        recommendation = self.calculate_recommendation(candles)
        if recommendation is None:
            recommendation = last_recommendation

        return recommendation, timestamp, price_variation

    def calculate_recommendation(self, original_candles):
        candles = original_candles[['open', 'low', 'high', 'close']].copy()
        candles = self.process_candles(candles)

        current_state = candles['signal'].iloc[-1]
        previous_state = candles['signal'].iloc[-2]
        change_of_state = current_state != previous_state
        
        recommendation = 1 if current_state == self.LONG else 0
        if self.use_signals:
            recommendation = None
            if current_state == self.CLOSE_LONG:
                recommendation = 0
            elif current_state == self.LONG and change_of_state:
                recommendation = 1

        return recommendation


    def process_candles(self, candles):
        candles['threshold'] = candles.low.rolling(24*2).mean().shift() * self.threshold_factor
        candles['low_threshold'] = candles.low.rolling(24*2).mean().shift() * self.low_threshold_factor
        candles['tendency'] = candles.open.dropna().rolling(self.n_for_tendency).apply(self.trend_classifier.classify_trend)
        candles['ma_100'] = candles.open.dropna().rolling(100).mean() * 1.0

        candles['signal'] = self.CLOSE_LONG # Por default estamos close.
        signal_col_number = candles.columns.get_loc("signal")
        
        i = 0
        avoid_bull = False
        bear_is_good = False # rojo justo dsps de cruzar el limite es bueno
        while i < len(candles):
            is_bull = (candles.tendency.iloc[i] == 1) and (candles.open.iloc[i] > candles.low_threshold.iloc[i])
            is_not_bull = candles.tendency.iloc[i] != 1
            is_not_bear = candles.tendency.iloc[i] != -1
            avoid_bull = False if is_not_bull else avoid_bull
            above_ma = (candles.low_threshold.iloc[i] > candles.ma_100.iloc[i])

            not_red_above_threshold = lambda x: (candles.tendency.iloc[x] != -1) and (candles.open.iloc[x] > candles.threshold.iloc[x])
            below_threshold = lambda x: (candles.open.iloc[x] < candles.threshold.iloc[x])

            if is_not_bull:
                avoid_bull = False

            if above_ma:
                already_long = False
                if i > 0:
                    already_long = (candles.signal.iloc[i-1] == self.LONG)
                already_long = already_long or (below_threshold(i-1) and not below_threshold(i))

                if ((is_bull and not avoid_bull) or not_red_above_threshold(i)) and already_long:
                    candles.iat[i,signal_col_number] = self.LONG

            if i > 0:
                if not_red_above_threshold(i-1) and below_threshold(i):
                    candles.iat[i,signal_col_number] = self.CLOSE_LONG
                    avoid_bull = True
                    
            i += 1
                        
        return candles

    def get_candles_needed_for_running(self):
        """ Returns the numbers of candles needed to process the strategy """
        return self.CANDLES_NEEDED_FOR_RUNNING


