import pandas as pd
import numpy as np
from quantstats.stats import (volatility, max_drawdown, drawdown_details, to_drawdown_series, expected_return)
import matplotlib.pyplot as plt

class Xmetrics():
    def get_investor_metrics(self, series_name: str, returns: pd.Series, periods) -> pd.DataFrame:
        """ calculates standards metrics for `returns`. Except for `max_drawdown` and `longest_drawdown`
        values are annualyzed.          
        """
        drawdown_series = to_drawdown_series(returns)
        exp_return = self.replace_undefined_values( expected_return(returns) , 0)
        annual_return = ((1 + exp_return)**periods) - 1
        daily_return = ((1 + annual_return)**((1/365))) - 1
        annual_volatility = volatility(returns, periods=periods)
        annual_neg_volatility = self.replace_undefined_values(self.calculate_negative_volatility(returns, periods), annual_volatility)

        return pd.DataFrame(
            index=[series_name],
            data = {
                'annual_return': annual_return,
                'daily_return': daily_return, 
                'volatility': annual_volatility,
                'neg_volatility': annual_neg_volatility,
                'sharpe': annual_return / annual_volatility,
                'sortino': annual_return / annual_neg_volatility,
                'max_drawdown': max_drawdown(returns),
                'longest_drawdown': drawdown_details(drawdown_series)['days'].max()
            }
        )
    
    def calculate_negative_volatility(self, returns, periods):
        mean = returns.mean()
        N = len(returns[returns<0])
        negative_variance = ((returns[returns<0]-mean)**2).sum()/(N-1)
        negative_volatility = np.sqrt(negative_variance)
        return negative_volatility*np.sqrt(periods) 
    
    def calculate_returns(self, df):
        '''
        Nota: Recordar que la columna "signal" tiene que tener los siguientes valores:
            1 = Para momentos en que la estrategia recomienda estar LONG
            0 = Para momentos en que la etrategia recomienda estar OUT
            -1 = Para momentos en que la estrategia recomienda estar SHORT
            
        Parameters
        ----------
        df : Pandas Dataframe con las columnas: open, high, low, close, returns y signal

        Returns
        -------
        df : Misma dataframe que el input pero con una columna de returns adicional

        '''
        if not 'signal' in df.columns:
            raise ValueError('No hay columna llamada "signal"')
        
        is_required_value = lambda x: x in [-1,0,1]
        signals_are_ok = df.signal.apply(is_required_value).all()        
        if not signals_are_ok:
            raise ValueError("Solo se aceptan -1,0, 1 en la columna signal")
        
        #cierro ultima posicion abierta para calcular como si fuese que cierro todo y me voy. Trades abiertos mezclan los numeros              
        df.loc[df.index[-1],'signal'] = 0
        
        # pongo los pct_change en returns por el momento
        df['returns'] = df.open.pct_change()
        df = df.fillna(0)    # <--- reemplaza el drop por el fillna
    
        # si estoy adentro (!=0), hay retornos...
        df['returns'] = np.where(df['signal'].shift() != 0., df['returns'], 0.)
        # ... pero los short tienen signo inverso, los cambio
        df['returns'] = np.where(df['signal'].shift() == -1, df['returns']*(-1), df['returns'])
    
        #comisiones de operatoria longs.  
        open_trade = (df['signal'] == 1) & (df['signal'].shift() != 1)
        close_trade = (df['signal'] != 1) & (df['signal'].shift() == 1)
        df["returns"] = np.where(open_trade.shift() | close_trade, (1+df["returns"])*(0.999)-1, df["returns"]) 
    
        #comisiones de operatoria short 
        open_trade = (df['signal'] == -1) & (df['signal'].shift() != -1)
        close_trade = (df['signal'] != -1) & (df['signal'].shift() == -1)
        df["returns"] = np.where(open_trade.shift() | close_trade, (1+df["returns"])*(0.999)-1, df["returns"]) 
    
        df.loc[df.index[0],"returns"] = 0. #Por que la linea anterior le cobra a la primer fila
    
        return df
        

    def get_operations(self, df) -> pd.DataFrame: 
        '''
        Nota: Recordar que la columna "signal" tiene que tener los siguientes valores:
            1 = Para momentos en que la estrategia recomienda estar LONG
            0 = Para momentos en que la etrategia recomienda estar OUT
            -1 = Para momentos en que la estrategia recomienda estar SHORT
        '''
        if not 'signal' in df.columns:
            raise ValueError('No hay columna llamada "signal"')
        is_required_value = lambda x: x in [-1,0,1]
        signals_are_ok = df.signal.apply(is_required_value).all()        
        if not signals_are_ok:
            raise ValueError("Solo se aceptan -1,0, 1 en la columna signal")
        if not 'returns' in df.columns:
            raise ValueError('No hay columna llamada "returns". Crearla considerando comisiones')
            
        df['performance'] = (1+df.returns).cumprod()        
            
        state_change = ~df['signal'].eq(df['signal'].shift())
        operations = df[state_change].copy()
        try:
            operations['returns'] = operations['performance'].pct_change().shift(-1)
            operations = operations.loc[operations.signal != 0]
            del operations['performance']
        except:
            pass
        
        return df, operations


    def strategy_metrics(self, df, periods, plot=False): 
        '''
        NOTA: Se recomienda primero tirar el metodo 'calculate_returns' para que se calculen correctamente los retornos considerando las comisiones.
        
        NOTA2: Recordar que la columna "signal" tiene que tener los siguientes valores:
            1 = Para momentos en que la estrategia recomienda estar LONG
            0 = Para momentos en que la etrategia recomienda estar OUT
            -1 = Para momentos en que la estrategia recomienda estar SHORT
            
        Parameters
        ----------
        df : Pandas Dataframe con las columnas: open, high, low, close, returns y signal
        periods : entero que referencia a la cantidad de observaciones en un anio. Si la data es horaria los periods son 8760 (365*24)
        plot : Bool para que grafique los histogramas adicionales a las metricas.

        Returns
        -------
        strategy_metrics : Pandas Datagrame con las metricas.

        '''
        if not 'signal' in df.columns:
            raise ValueError('No hay columna llamada "signal"')
        is_required_value = lambda x: x in [-1,0,1]
        signals_are_ok = df.signal.apply(is_required_value).all()        
        if not signals_are_ok:
            raise ValueError("Solo se aceptan -1,0, 1 en la columna signal")
        
        df, operations = self.get_operations(df)
        if len(operations) == 0:
            return self.get_dummy_metrics()
        
        xmetrics = self.get_investor_metrics('Estrategia', 
                             df["returns"],
                             periods).T
        
        mean_gain_return, mean_loss_return, mean_gainloss_ratio = self.calculate_gain_loss_ratio(operations)
        
        more_metrics = pd.DataFrame(
                index= ['Estrategia'],
                data = {
                    'accumulated_return_hold': df.open[-1]/df.open[0]-1,
                    'accumulated_return_strategy': (df.returns+1).cumprod()[-1]-1,
                    'total_trades': len(operations),  
                    'winners': operations[operations.returns > 0].shape[0], 
                    'losers': operations[operations.returns < 0].shape[0],
                    'mean_gain_return': mean_gain_return,
                    'mean_loss_return': mean_loss_return,
                    'mean_gain/loss_ratio': mean_gainloss_ratio,
                    'win_probability': self.replace_undefined_values((operations[operations.returns > 0].shape[0]) / (len(operations)), 0)
                }
            ).T
        strategy_metrics = pd.concat([xmetrics, more_metrics], axis=0)
       
        xratio = pd.DataFrame({'xratio': strategy_metrics.loc['win_probability'] * ( 1-np.exp( 1-strategy_metrics.loc['mean_gain/loss_ratio']) ) / strategy_metrics.loc['neg_volatility'] })
        strategy_metrics = pd.concat([strategy_metrics, xratio.T], axis=0)
               
        if plot == True:        
            self.plot_returns_per_trade(operations)
                
        return strategy_metrics
    
    def plot_returns_per_trade(self, operations):
        operations['daily_returns'] = 0.
        
        for i in range(0, len(operations)-1):
            date_1 = operations.index[i]
            date_2 = operations.index[i+1]     
            time_delta = (date_2 - date_1)
            total_seconds = time_delta.total_seconds()
            days = total_seconds/60/60/24
            operations.loc[[date_1],['daily_returns']] = (operations['returns'][i] +1)**(1/days) -1
    
        self.plot_personalized_histogram(operations.loc[operations['returns'] != 0,'returns'], 
                                    title = 'Histograma de retornos acumulados por trade', 
                                    xlabel = 'Retornos acumulados por trade')
        self.plot_personalized_histogram(operations.loc[operations['daily_returns'] != 0,'daily_returns'], 
                                    title = 'Histograma de retornos diarios por trade', 
                                    xlabel = 'Retornos diarios por trade')
   
    
    def plot_personalized_histogram(self, data_to_plot, title:str, xlabel:str):
        n, bins, patches = plt.hist(x=data_to_plot, bins=10, color='#0504aa',
                                alpha=0.7, rwidth=0.85)
        plt.grid(axis='y', alpha=0.75)
        plt.xlabel(xlabel)
        plt.ylabel('Frecuencia')
        plt.title(title)
        maxfreq = n.max()
        # Set a clean upper y-axis limit.
        plt.ylim(ymax=np.ceil(maxfreq / 10) * 10 if maxfreq % 10 else maxfreq + 10)
        plt.show()
        
    def get_dummy_metrics(self):
        index = ['annual_return','daily_return','volatility','neg_volatility',
                 'sharpe','sortino','max_drawdown','longest_drawdown','accumulated_return_hold',
                 'accumulated_return_strategy','total_trades','winners','losers','mean_gain_return',
                 'mean_loss_return','mean_gain/loss_ratio','win_probability','xratio']
        data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        return pd.DataFrame(index=index, data=data, columns=['Estrategia'])
        
    def replace_undefined_values(self, value, exception_value):
        if (np.isnan(value)) or (np.isinf(value)):
            return exception_value
        return value
            
    def calculate_gain_loss_ratio(self, operations):
        mean_gain_return = self.replace_undefined_values( operations[operations.returns > 0].returns.mean(), 0)
        mean_loss_return = self.replace_undefined_values( -operations[operations.returns < 0].returns.mean(), 0)
        if  (mean_loss_return == 0) and (mean_gain_return >= 0): #No hay losers
            mean_gainloss_ratio = 1.7
        else: 
            mean_gainloss_ratio = mean_gain_return / mean_loss_return
        return mean_gain_return, mean_loss_return, mean_gainloss_ratio