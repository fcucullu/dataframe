from bayes_opt import BayesianOptimization
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from time import time

def bayes_optimize(strategy, 
                   samples,
                   bounds, 
                   acq_func, 
                   init_points=2, 
                   n_iter = 98, 
                   plot=False,
                   verbose=2):
    
    init_time = time()
    def objective_function(**kwargs):
        return np.median([strategy(sample).get_performance(**kwargs) for sample in samples])
            
    optimizer = BayesianOptimization(objective_function,
                                       pbounds = bounds,
                                       random_state=0,
                                       verbose=verbose)
    optimizer.maximize(init_points=init_points,
                       n_iter=n_iter,
                       acq=acq_func)
    end_time = time()
    
    df = pd.DataFrame()
    df['strategy'] = [strategy.__class__.__name__]
    df['acq_func'] = [acq_func]
    df['performance'] = [optimizer.max['target']]
    df['params'] = [optimizer.max['params']]
    df['delay'] = [end_time - init_time]

    if plot:
        print("Best result: {}; f(x) = {:.3f}.".format(optimizer.max["params"], optimizer.max["target"]))
        
        plt.figure(figsize = (15, 5))
        plt.plot(range(1, 1 + len(optimizer.space.target)), optimizer.space.target, "-o")
        plt.grid(True)
        plt.xlabel("Iteration", fontsize = 14)
        plt.ylabel("Performance f(x)", fontsize = 14)
        plt.xticks(fontsize = 14)
        plt.yticks(fontsize = 14)
        plt.show()
    
    return df










