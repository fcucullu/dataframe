import os
from utils.candlestick import BinancePricesRepository
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import pandas as pd
import numpy as np
from utils.simulations import generate_simulation
import utils.optimizators as opt
import warnings
warnings.filterwarnings("ignore")

from strategies.TwoStandardMovingAverage import TwoStandardMovingAverage
from strategies.BBvolumeStrategy import BBvolumeStrategy
from strategies.VWAPvsSMA import VWAPvsSMA
from strategies.Holding import Holding


Repository = BinancePricesRepository()


###PERIOD TOP STUDY !###
start_date = datetime(2020,6,1)
end_date = datetime(2023,4,13)
limit = ((end_date - start_date).days+1)*6
base = 'ETH'
quote = 'USDT'
###PERIOD TOP STUDY !###
candles_4h = Repository.get_candles(base, quote, '4h',limit=limit, end_time=end_date)
#candles_4h = sim.FillNa().fill_ohlc(candles_4h)





candles_train, candles_test = (
    candles_4h.iloc[:int(0.8*len(candles_4h))], candles_4h.iloc[int(0.8*len(candles_4h)):])


periods_in_weeks = 2
strategies = [Holding,
              TwoStandardMovingAverage, 
#              ThreeStandardMovingAverage, 
 #             ThreeStandardMovingAverageAlternative, 
  #            ChandelierExitStrategy, 
              VWAPvsSMA, 
              BBvolumeStrategy
          ]

dates = [datetime(2022,1,1),end_date]
date = datetime(2022,1,5)
while date < end_date:
    if date.day == 5:
        dates.append(date)
        date = date + timedelta(days=15)
    if date.day == 20:
        dates.append(date)
        date = date - timedelta(days=15)
        date = date + relativedelta(months=1)
            
dates.sort()
dates = dates[:-1]
    
results = []
optimal_params = []
for i, date in enumerate(dates[1:-1]):
    print('validating on date:',date)
    start_date = date - timedelta(days=500)
    candles_train, candles_test = (
        candles_4h.loc[start_date:date], candles_4h.loc[date:dates[i+2]])    
    train_sim = generate_simulation(candles_train,240,base,quote,num_sim=100,sim_length=1000)
    results_per_strategy = {'index':date}
    params_per_strategy = {'index':date}
    
    for strategy in strategies:
        
        if strategy.__name__ != 'Holding':
            df = opt.bayes_optimize(strategy,
                                    train_sim,
                                    strategy.STANDARD_BOUNDS,
                                    'ucb',
                                    init_points=150, 
                                    n_iter = 30,
                                    verbose=0,
                                    )
            params = df.loc[0,'params']
            params_per_strategy.update({strategy.__name__ :params})
        else:
            params = {}
        dict_strategy_return = {strategy.__name__ :strategy(candles_test).get_performance(**params)}
        results_per_strategy.update(dict_strategy_return)
        print(dict_strategy_return)
    results.append(results_per_strategy)
    optimal_params.append(params_per_strategy)
    
first_row = {strategy.__name__ :1 for strategy in strategies}
first_row.update({'index':dates[0]})
results.append(first_row)



strategies_params = pd.DataFrame(optimal_params).set_index('index')
strategies_params = strategies_params.sort_index()

strategies_returns = pd.DataFrame(results).set_index('index')
strategies_returns = strategies_returns.sort_index()
strategies_performance = strategies_returns.cumprod()

strategies_performance.plot()

if not os.path.exists('optimal_strategies'):
    os.makedirs('optimal_strategies')
    
try:
    df = pd.read_csv("optimal_strategies/params.csv",index_col='index')
    for col in strategies_params.columns:
        df[col] = strategies_params[col]
    df.to_csv("optimal_strategies/params.csv")
except:
    strategies_params.to_csv("optimal_strategies/params.csv")
    
try:
    df = pd.read_csv("optimal_strategies/returns.csv",index_col='index')
    for col in strategies_returns.columns:
        df[col] = strategies_returns[col]
    df.to_csv("optimal_strategies/returns.csv")
except:
    strategies_returns.to_csv("optimal_strategies/returns.csv")
    



#sin legenda
#for s in strategies_performance.columns:
#    strategies_performance[s].plot()


