from strategies.Holding import Holding
import numpy as np

class ChandelierExitStrategy(Holding): 
    STANDARD_BOUNDS = {'mean': [10,30],
                       'chand_window': [5,15],
                       'mult_high': [0.01,5],
                       'mult_low': [0.01,5]}
    
    def calculate_true_range(self):
        candles = self.candles
        candles['tr1'] = candles["high"] - candles["low"]
        candles['tr2'] = abs(candles["high"] - candles["close"].shift(1))
        candles['tr3'] = abs(candles["low"] - candles["close"].shift(1))
        candles['TR'] = candles[['tr1','tr2','tr3']].max(axis=1)
        candles.loc[candles.index[0],'TR'] = 0
        return candles

    def calculate_average_true_range(self, mean):
        candles = self.calculate_true_range()
        candles['ATR'] = 0
        candles.loc[candles.index[mean],'ATR'] = round( candles.loc[candles.index[1:mean+1],"TR"].rolling(window=mean).mean()[-1], 4)
        const_atr = (mean-1)/mean
        const_tr = 1/mean
        ATR=candles["ATR"].values
        TR=candles["TR"].values
        for index in range(mean+1, len(candles)):
            ATR[index]=ATR[index-1]*const_atr+TR[index]*const_tr
        candles["ATR"]=ATR
        return candles
    
    def calculate_chandelier_exits(self, mean, chand_window, mult_high, mult_low):
        candles = self.calculate_average_true_range(mean)
        candles["chandelier_high"] = candles['close']
        candles["chandelier_low"] = candles['close']
        candles.loc[candles.index[chand_window+1:],"chandelier_low"] = candles.loc[candles.index[chand_window+1:],"low"].rolling(chand_window).min() + mult_low * candles["ATR"][chand_window+1:]
        candles.loc[candles.index[chand_window+1:],"chandelier_high"] = candles.loc[candles.index[chand_window+1:],"high"].rolling(chand_window).max() - mult_high * candles["ATR"][chand_window+1:]
        return candles

    def process_candles(self, mean, chand_window, mult_high, mult_low):
        mean = int(mean)
        chand_window = int(chand_window)
        candles = self.candles
        
        candles = self.calculate_chandelier_exits(mean, chand_window, mult_high, mult_low)
        candles["signal"] = 0
        candles["signal"] = np.where(candles["close"] > candles["chandelier_low"], 1, candles["signal"])
        candles["signal"] = np.where(candles["close"] < candles["chandelier_high"], -1, candles["signal"])
        candles["signal"] = candles["signal"].shift(1).fillna(0) 
        return candles