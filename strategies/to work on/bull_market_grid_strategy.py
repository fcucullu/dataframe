import pandas as pd
import numpy as np
from datetime import datetime
import pytz
from stockstats import StockDataFrame

from recommendations.strategies.candlestick import BinancePricesRepository
from recommendations.strategies.pair_strategies import Holding


class BullMarketGridStrategy(Holding):

    CANDLES_NEEDED_FOR_RUNNING = {
        60: 720, 
        30: 1440,
        15: 2880,
        5: 15000}  # One month

    LONG = 1
    CLOSE_LONG = 0

    def __init__(self, 
        base_currency: str, 
        quote_currency: str,
        time_frame: int, 
        active_operation_threshold: float = 0,
        mean_atr: int = 6,
        loss_multiple: int = 2,
        gain_multiple: int = 2,
        prices_repository=BinancePricesRepository()):
        super().__init__(base_currency, quote_currency, time_frame, prices_repository)         
        self.active_operation_threshold = active_operation_threshold
        self.mean_atr = int(mean_atr)
        self.loss_multiple = loss_multiple
        self.gain_multiple = gain_multiple

    def run(self, last_recommendation:float = 0):
        candles = self.get_candles_data()
        timestamp = candles.index[-1]
        price_variation = self.calculate_prices_variation(candles['open'])

        if self.current_price_is_below_active_operation_threshold(candles):
            recommendation = self.CLOSE_LONG
        else:
            recommendation = self.calculate_recommendation(candles)

        return recommendation, timestamp.replace(tzinfo=pytz.utc), price_variation

    def current_price_is_below_active_operation_threshold(self, candles):
        return candles.open.iloc[-1] < self.active_operation_threshold

    def calculate_recommendation(self, original_candles):
        candles = self.process_action(original_candles)
        candles = self.process_action_grid(candles)

        if candles['topped_action'].iloc[-1] != self.CLOSE_LONG:
            recommendation = candles['topped_action'].iloc[-1]
        elif candles['topped_action'].iloc[-1] == self.CLOSE_LONG:
            recommendation = 0
        else:
            raise ValueError("Action is not defined")
        
        return recommendation

    def process_action(self, original_candles):
        df_minute = original_candles.copy()
        df_minute['hour'] = [df_minute.index[i].replace(minute=0) for i in range(len(df_minute))]
        df_hour = df_minute[df_minute.index.minute == 0]
        
        
        df_hour['ema8'] = df_hour['close'].ewm(span=8).mean()
        df_hour['ema21'] = df_hour['close'].ewm(span=21).mean()
        df_hour['ema100'] = df_hour['close'].ewm(span=100).mean()
        stockdata = StockDataFrame.retype(df_hour)
        df_hour['ADX'] = stockdata.get('adx')
        
        df_hour['condition_directional'] = ( df_hour['close'] > df_hour['ema100'] )
        df_hour['condition_lateral'] = ( df_hour['ADX'] > 25 )
        df_hour['condition_long'] = ( df_hour['ema8'] > df_hour['ema21'] )
        df_hour['above_threshold'] = ( df_hour['close'] > self.active_operation_threshold )
        
        buy_order = (
            df_hour['condition_directional']
            & df_hour['condition_lateral']
            & df_hour['condition_long']
            & df_hour['above_threshold'])

        df_hour['basic_action'] = np.where(
            buy_order,
            self.LONG, 
            self.CLOSE_LONG, 
        )
                
        cols_to_use = df_hour.columns.difference(df_minute.columns)
        df_minute = df_minute.merge(df_hour[cols_to_use], left_on='hour', right_index=True)

        df_minute['basic_action'] = df_minute['basic_action'].shift(1).ffill().replace(np.nan,0)
        return df_minute

    def process_action_grid(self, candles):
        df_minute = candles.copy()
        df_minute['hour'] = [df_minute.index[i].replace(minute=0) for i in range(len(df_minute))]
        df_hour = df_minute[df_minute.index.minute == 0]
        df_hour['atr'] = self.calculate_atr(df_hour)
        
        cols_to_use = df_hour.columns.difference(df_minute.columns)
        df_minute = df_minute.merge(df_hour[cols_to_use], left_on='hour', right_index=True)
        
        #Start section to shift all df_hour-based indicators
        df_minute['atr'] = df_minute['atr'].shift(1).ffill().replace(np.nan,0)
        #End section
        
        df_minute['sl'] = np.inf
        df_minute['lvl2'] = np.inf
        df_minute['lvl3'] = np.inf
        trades = self.detect_trades(df_minute)
        for start_trade, end_trade in trades:
            for row in range(start_trade, end_trade):
                df_minute['sl'][row] = df_minute['open'][start_trade:row+1].max() - self.loss_multiple * df_minute['atr'][start_trade]
            df_minute['lvl2'][start_trade: end_trade] = df_minute['open'][start_trade] + self.gain_multiple * df_minute['atr'][start_trade]
            df_minute['lvl3'][start_trade: end_trade] = df_minute['open'][start_trade] + 2*self.gain_multiple * df_minute['atr'][start_trade]
        df_minute['reentry'] = df_minute['sl'].copy()
        
        df_minute = self.calculate_conditions(df_minute)
        
        #new re-entry condition
        #Entry at last_max and SL at last_exit
        for start_trade, end_trade in trades:
            for row in range(start_trade, end_trade):
                if (df_minute['condition_sl'][row] == False) and (df_minute['condition_sl'][row-1] == True):
                    df_minute['reentry'][row: end_trade] = df_minute['open'][start_trade:row+1].max()
                    df_minute['lvl2'][row: end_trade] = df_minute['reentry'][row] + self.gain_multiple * df_minute['atr'][row]
                    df_minute['lvl3'][row: end_trade] = df_minute['reentry'][row] + 2*self.gain_multiple * df_minute['atr'][row]
                    df_minute['sl'][row:end_trade] = [max(df_minute['reentry'][i],
                                                             df_minute['open'][i] - self.loss_multiple * df_minute['atr'][row]) for i in range(row, end_trade)]

        df_minute = self.calculate_conditions(df_minute)

        weights_and_conditions = [
                      (.33, df_minute['condition_sl'] & df_minute['condition_re'] & ~df_minute['condition_2'] & ~df_minute['condition_3']),
                      (.66, df_minute['condition_sl'] & df_minute['condition_re'] &  df_minute['condition_2'] & ~df_minute['condition_3']),
                      (1  , df_minute['condition_sl'] & df_minute['condition_re'] &  df_minute['condition_2'] &  df_minute['condition_3']),
                    ]

        df_minute['topped_action'] = 0
        for weight, condition in weights_and_conditions:
            df_minute['topped_action'] = np.where(condition, weight, df_minute['topped_action'])

        return df_minute
    
    def calculate_conditions(self, df):
        df['condition_re'] = df['open'] > df['reentry']
        df['condition_sl'] = df['open'] > df['sl']
        df['condition_2'] = df['open'] > df['lvl2']
        df['condition_3'] = df['open'] > df['lvl3']
        return df


    def calculate_atr(self, candles):
        df = candles.copy()
        df['h-l'] = df['high'] - df['low']
        df['h-c'] = df['high'] - df['close'].shift(1)
        df['l-c'] = df['low'] - df['close'].shift(1)
        df['max'] = df[['h-l', 'h-c', 'l-c']].max(axis=1)
        df['atr'] = df['max'].rolling(window=self.mean_atr).mean()
        return df['atr'].shift()

    def detect_trades(self, candles):
        df = candles.copy()
        trades = np.where(np.diff(np.sign(df['basic_action'])))[0]
        if len(trades)%2 != 0:
            trades = np.append(trades, len(df)-1)
        trades = trades.reshape(int(len(trades)/2), 2)
        return trades + 1  

    def get_candles_needed_for_running(self):
        """ Returns the numbers of candles needed to process the strategy """
        return self.CANDLES_NEEDED_FOR_RUNNING[self.time_frame]
