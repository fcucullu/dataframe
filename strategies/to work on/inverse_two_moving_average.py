import os
import pathlib
import pandas as pd

from recommendations.strategies.pair_strategies import Holding
from recommendations.strategies.candlestick import BinancePricesRepository, MINUTES_TO_BINANCE_INTERVAL


STRATEGIES_DATA_FOLDER = os.path.join(pathlib.Path(__file__).parent.absolute(), 'strategies_data')

class InverseTwoMovingAverage(Holding):
    """
    When short ma is less than long we go long, otherwise we go short/out_of_market. 
    Given a position if we "lose" the X percentage (`pct_to_change_position`), 
    we are in the wrong position, then we change it.  
    """
    LONG = 'long'
    SHORT = 'short'
    REVERSE_LONG = 'reverse_long'
    REVERSE_SHORT = 'reverse_short'


    def __init__(self, base_currency: str, quote_currency: str,
                 time_frame: int,
                 prices_repository=BinancePricesRepository(),**kwargs):
        super().__init__(base_currency, quote_currency, time_frame, prices_repository)

        self.ma_long = int(kwargs['ma_long'])
        self.ma_short = int(kwargs['ma_short'])
        self.pct_to_change_position = kwargs['pct_to_change_position']
        file_name = f'inv_moving_average_{base_currency}_{quote_currency}_{self.ma_short}_{self.ma_long}_{time_frame}.csv'
        self.file = os.path.join(STRATEGIES_DATA_FOLDER, file_name)
        if not os.path.isfile(self.file):
            with open(self.file, 'x') as f:
                f.write('timestamp,signal,open_price\n')


    def run(self, last_recommendation=0):
        candles_data = self.get_candles_data()
        open_prices = candles_data.open 
        signal = self.get_signal(open_prices)
        timestamp = open_prices.index[-1]
        current_price = open_prices[-1]
        last_signal, last_operation_price = self.get_last_operation_data()

        proportion_to_hold = last_recommendation
        if signal:
            self.log_operation(timestamp, signal, current_price) 
            proportion_to_hold = 1 if signal == self.LONG else 0
        else:
            if last_signal == self.LONG:
                price_to_change_position = last_operation_price * (1 - self.pct_to_change_position) 
                if current_price < price_to_change_position:
                    signal = self.REVERSE_LONG
                    self.log_operation(timestamp, signal, current_price)                     
                    proportion_to_hold = 0
            elif last_signal == self.SHORT:
                price_to_change_position = last_operation_price * (1 + self.pct_to_change_position) 
                if current_price > price_to_change_position:
                    signal = self.REVERSE_SHORT
                    self.log_operation(timestamp, signal, current_price) 
                    proportion_to_hold = 1

        return proportion_to_hold, timestamp, self.calculate_prices_variation(open_prices)


    def get_signal(self, open_prices):
        long = open_prices[-self.ma_long:].mean()
        short = open_prices[-self.ma_short:].mean()

        prev_long = open_prices[-(self.ma_long+1):-1].mean()
        prev_short = open_prices[-(self.ma_short+1):-1].mean()

        state = short > long
        prev_state = prev_short > prev_long

        signal = None
        if prev_state != state:
            signal = self.LONG 
            if short > long:
                signal = self.SHORT

        return signal

    def get_last_operation_data(self):
        operations = pd.read_csv(self.file, index_col=0, parse_dates=['timestamp'])

        last_signal = None
        last_operation_price = None 
        if len(operations) > 0:
            last_signal = operations.signal[-1]
            last_operation_price = operations.open_price[-1]
        
        return last_signal, last_operation_price


    def get_candles_needed_for_running(self):
        """ Returns the numbers of candles needed to process the strategy """
        return self.ma_long + 1

    def log_operation(self, timestamp, signal, price):
        with open(self.file, 'a') as f:
            f.write(f'{timestamp},{signal},{price}\n')
