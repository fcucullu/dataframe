import pandas as pd
import numpy as np
from decimal import Decimal as D
from ta.volatility import bollinger_lband,bollinger_hband, keltner_channel_lband,keltner_channel_hband
from ta.trend import ADXIndicator
from recommendations.strategies.candlestick import BinancePricesRepository, MINUTES_TO_BINANCE_INTERVAL
from market_profile import MarketProfile
from recommendations.strategies.pair_strategies import Holding
from datetime import datetime, timedelta
from recommendations.indicators.volume_profile import VolumeProfileIndicator
from recommendations.indicators.get_trend_by_sq import GetTrend

class VolumeProfileSecondVersion(Holding):
    
    MAX_NUMBER_OF_CANDLES = 1000 
    N_CANDLES_OPEN_SMA_SHORT = 3
    N_CANDLES_OPEN_SMA_LONG = 6
    N_CANDLES_SMA_SHORT_VP = 7     
    
    def __init__(self, 
        base_currency: str, 
        quote_currency: str,
        time_frame: int, 
        factor_volume: float,
        factor_spike: float,
        n_rolling_volume: int,
        check_trend: bool,
        prices_repository=BinancePricesRepository()):

        super().__init__(base_currency, quote_currency, time_frame, prices_repository)         
        self.factor_volume = factor_volume
        self.factor_spike = factor_spike
        self.n_rolling_volume = int(n_rolling_volume)
        self.check_trend = check_trend

    ## Run
    def run(self, last_recommendation: float =0, **kwargs):
        """ 
        return strategy status. Value 1 means hold, 0 means do not hold. 
        """
        
        candles_data = self.get_candles_data()
        timestamp = candles_data.index[-1]    
        
        price_variation = self.calculate_prices_variation(candles_data['open'])
        recommendation = self.calculate_recommendation(candles_data, last_recommendation)

        return recommendation, timestamp, price_variation

    ## Calculate Reco  
    def calculate_recommendation(self, candles, last_recommendation): 

        reco = self.get_strategy(candles, self.factor_volume, 
                                   self.factor_spike, self.n_rolling_volume)
        
        recommendation = last_recommendation
        if reco==1:
            recommendation = 1
        elif reco==0:
            recommendation = 0

        return recommendation
        
    #Get main strategy
    def get_strategy(self, candles, factor_volume, factor_spike, n_volume):

        df_temp = self.get_data(candles, factor_volume, factor_spike, n_volume)    

        ### criteria        
        hold_above_sma = df_temp.open_prices[-1] > df_temp.sma_100[-1]

        hold_va_high = (df_temp.open_prices[-1] > df_temp.va_high[-1])\
                    & (df_temp.open_prices[-2] > df_temp.va_high[-2])\
                    & (df_temp.open_prices[-3] > df_temp.va_high[-3])\
                    & (df_temp.open_prices[-1] > df_temp.short_high_sma[-1])\
                    & (df_temp.open_prices[-2] > df_temp.short_high_sma[-1])\

        hold_volume = (df_temp.volume_shift[-1] > factor_volume * df_temp.volume_shift_sma[-1])

        no_spikes = df_temp.spike[-1] != 1

        #Sell        
        not_hold_long_short = (df_temp.open_prices[-1] < df_temp.short_low_sma[-1])\
                            & (df_temp.open_prices[-1] < df_temp.short_low_sma[-1])\
                            & (df_temp.open_prices_pct[-1] > -0.05)

        not_hold_low = (df_temp.open_prices[-1]<df_temp.sma_100[-1]) & (df_temp.touching_sma100[-1] == 0)     
        
        hold = hold_va_high & hold_volume & no_spikes & hold_above_sma 
        
        if self.check_trend:
            gt = GetTrend(self.base_currency, self.quote_currency, self.time_frame, df_temp.index[0], df_temp.index[-1])
            df_sq = gt.trend_squezee_adx()
            df_temp['trend_sq'] = df_sq['trend']
            trend_bull = (df_temp['trend_sq'][-1] ==1)
            hold = hold & trend_bull

        not_hold = not_hold_long_short | not_hold_low
                
        recom = None
        if hold:
            recom = 1
        if not_hold:
            recom = 0
                   
        return recom
                   
  
    def get_data(self, time_serie, factor_volume, factor_spike, n_volume):
   
        df_temp = pd.DataFrame()

        df_temp['open_prices'] = time_serie["open"]
        df_temp['close_prices'] = time_serie["close"]
        df_temp['low_prices'] = time_serie["low"]
        df_temp['high_prices'] = time_serie["high"]
        df_temp['open_prices_pct'] = time_serie["open"].pct_change()
        df_temp['volume'] = time_serie["volume"]

        df_temp["open_sma_short"] = df_temp['open_prices'].rolling(window=self.N_CANDLES_OPEN_SMA_SHORT).mean()
        df_temp["open_sma_long"] = df_temp['open_prices'].rolling(window=self.N_CANDLES_OPEN_SMA_LONG).mean()

        #get volume profile
        volume_profiles = VolumeProfileIndicator(self.base_currency, 
                                                 self.quote_currency, 
                                                 self.time_frame, 
                                                 2, 0.5, 0.5,
                                                 df_temp.index[0],
                                                 df_temp.index[-1])
        
        vp, vp_short = volume_profiles.get_volume_profiles()
        
        #get df complete
        for cols in vp.columns:
            df_temp.loc[vp.index, cols] = vp[cols]
        df_temp = df_temp.fillna(method='ffill')

        df_temp['short_low'] = vp_short.va_low
        df_temp['short_low_sma'] = df_temp['short_low'].rolling(window=self.N_CANDLES_SMA_SHORT_VP).mean()    
        df_temp['short_high_sma'] = vp_short['va_high'].rolling(window=self.N_CANDLES_SMA_SHORT_VP).mean() 

        df_temp['volume_shift'] = df_temp['volume'].shift(1)
        df_temp['volume_shift_sma'] = df_temp['volume_shift'].rolling(window=n_volume).mean() 

        df_temp['diff_open_close'] = df_temp[['open_prices', 'close_prices']].max(axis=1) - df_temp[['open_prices', 'close_prices']].min(axis=1)
        
        df_temp['spike_up'] = df_temp['high_prices'] - df_temp[['open_prices', 'close_prices']].max(axis=1)
        df_temp['spike_down'] = df_temp[['open_prices', 'close_prices']].min(axis=1) - df_temp['low_prices']                                           
        df_temp['spike'] = np.where((df_temp['spike_up'] > factor_spike*df_temp['diff_open_close']) | (df_temp['spike_down'] > factor_spike*df_temp['diff_open_close']), 1, 0)
        df_temp['spike'] =  df_temp['spike'].shift(1)
        df_temp['sma_high'] = df_temp['high_prices'].rolling(window=4).mean()

        ### sma 100
        df_temp['sma_100'] = df_temp['open_prices'].rolling(window=100).mean()

        #If candles take the sma_100
        df_temp['touching_sma100'] = np.where((df_temp['high_prices'] < df_temp['sma_100']) | (df_temp['low_prices'] > df_temp['sma_100']), 0, 1 )

        return df_temp

    def get_candles_needed_for_running(self):
        return self.MAX_NUMBER_OF_CANDLES