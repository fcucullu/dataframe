import pandas as pd
import numpy as np
from decimal import Decimal as D
from ta.volatility import bollinger_lband,bollinger_hband, keltner_channel_lband,keltner_channel_hband
from ta.momentum import rsi    
from ta.trend import adx, adx_neg, adx_pos, macd_diff
from recommendations.strategies.candlestick import BinancePricesRepository, MINUTES_TO_BINANCE_INTERVAL
from market_profile import MarketProfile
from recommendations.strategies.pair_strategies import Holding
from datetime import datetime, timedelta

class VolumeProfileModifiedStrategy(Holding):

    max_number_of_candles = 1000 #mayuscula
    
    def __init__(self, 
        base_currency: str, 
        quote_currency: str,
        time_frame: int, 
        bb_periods: int, 
        bb_std_dev: float, 
        n_candles_open_sma_short: int,
        n_candles_open_sma_long: int,
        n_candles_sma_short_vp: int,
        n_candles_sma_long_vp: int,
        threshold_bb_rate: float,
        prices_repository=BinancePricesRepository()):

        super().__init__(base_currency, quote_currency, time_frame, prices_repository)         
        self.bb_periods = int(bb_periods)
        self.bb_std_dev = bb_std_dev
        self.n_candles_open_sma_short = int(n_candles_open_sma_short)
        self.n_candles_open_sma_long = int(n_candles_open_sma_long)
        self.n_candles_sma_short_vp = int(n_candles_sma_short_vp)
        self.n_candles_sma_long_vp = int(n_candles_sma_long_vp)
        self.threshold_bb_rate = threshold_bb_rate
        
    def run(self, last_recommendation: float =0, **kwargs):
        """ 
        return strategy status. Value 1 means hold, 0 means do not hold. 
        """

        ##### Precios #####
        candles_data = self.get_candles_data()
        candles_data = self.increase_values_to_significant_magnitudes(candles_data, self.quote_currency)

        open_prices = candles_data.open
        open_prices_pct = open_prices.pct_change()
        ###################
        
        ##### Variables #####
        current_variables = self.get_variables(open_prices)
        current_bb_rate = current_variables['bb_rate']
        current_open_sma_short = current_variables['open_sma_short'] 
        open_sma_short_shift_1 = current_variables['open_sma_short_shift_1'] 
        current_open_sma_long = current_variables['open_sma_long']
        open_sma_long_shift_1 = current_variables['open_sma_long_shift_1']
        current_bb_hband = current_variables['bb_hband']
        #####################
        
        ##### Volumen Profile #####
        timestamp = open_prices.index[-1] #current timestamp
        timestamp_shift_1 = open_prices.index[-2] #previous timestamp
        timestamp_shift_2 = open_prices.index[-3] #before previous timestamp
        
        ### VolumeProfile del día anterior(ayer), usado en la fecha actual timestamp
        daily_vp = self.get_yesterday_market_profile(timestamp,0)
        daily_poc_shift_0 = daily_vp['poc'] 
        daily_va_low_shift_0 = daily_vp['va_low'] 
        daily_va_high_shift_0 = daily_vp['va_high']
        #####################
    
        ### VolumeProfile del día anterior al anterior(antes de ayer), usado en la fecha actual
        yesterday_vp = self.get_yesterday_market_profile(timestamp, 1)
        yesterday_poc_shift_0 = yesterday_vp['poc'] 

        before_yesterday_vp = self.get_yesterday_market_profile(timestamp, 2)
        before_yesterday_poc_shift_0 = before_yesterday_vp['poc'] 
        #####################
        
        if timestamp_shift_1.day == timestamp.day:
            daily_va_high_shift_1 = daily_va_high_shift_0
            daily_poc_shift_1 = daily_poc_shift_0
            if timestamp_shift_2.day == timestamp_shift_1.day:
                daily_va_high_shift_2 = daily_va_high_shift_0
                #daily_poc_shift_2 = daily_poc_shift_0
            else:
                daily_vp_shift_2 = self.get_yesterday_market_profile(timestamp_shift_2,0)
                daily_va_high_shift_2 = daily_vp_shift_2['va_high']
                #daily_poc_shift_2 = daily_vp_shift_2['poc']
        else:
            daily_vp_shift_1 = self.get_yesterday_market_profile(timestamp_shift_1,0)
            daily_va_high_shift_1 = daily_vp_shift_1['va_high']
            daily_poc_shift_1 = daily_vp_shift_1['poc']
            daily_va_high_shift_2 = daily_va_high_shift_1
            #daily_poc_shift_2 = daily_poc_shift_1

            
        #datos de "2 dias"
        vp_short_sma = self.get_market_profile(5*12*24*2, timestamp, number_of_rolling=self.n_candles_sma_short_vp) 
        current_short_poc_sma = vp_short_sma['poc']
        current_short_low_sma = vp_short_sma['va_low'] 
        current_short_high_sma = vp_short_sma['va_high']

        vp_short_sma_shift_1 = self.get_market_profile(5*12*24*2, timestamp_shift_1, number_of_rolling=self.n_candles_sma_short_vp) 
        #short_poc_sma_shift_1 = vp_short_sma_shift_1['poc']
        short_low_sma_shift_1 = vp_short_sma_shift_1['va_low'] 
        #short_high_sma_shift_1 = vp_short_sma_shift_1['high']

 
        #datos de "3 dias"
        vp_long_sma = self.get_market_profile(5*12*24*3, timestamp, number_of_rolling=self.n_candles_sma_long_vp) 
        current_long_poc_sma = vp_long_sma['poc']
        #current_long_low_sma = vp_long_sma['low'] 
        current_long_high_sma = vp_long_sma['va_high']    

        vp_long = self.get_market_profile(5*12*24*3, timestamp, number_of_rolling=1) 
        current_long_poc = vp_long['poc']
        #####################

        #el poc de hoy con el del dia anterior (diarios)

        daily_poc_pct = (daily_poc_shift_0 - yesterday_poc_shift_0)/yesterday_poc_shift_0 
        daily_poc_pct_shift_1 = (yesterday_poc_shift_0 - before_yesterday_poc_shift_0)/before_yesterday_poc_shift_0 

        umbral_poc_pct = 0.01
        umbral_open_pct = -0.05

        #Buy
        hold_va_high = (open_prices[-1]>daily_va_high_shift_0)\
                    and (open_prices[-2]>daily_va_high_shift_1)\
                    and (open_prices[-3]>daily_va_high_shift_2)\
                    and (open_prices[-1]<current_bb_hband)\

        hold_long_short = (open_prices[-1]>current_open_sma_short)and(open_prices[-2]>open_sma_short_shift_1)\
                    and (open_prices[-1]>current_open_sma_long)and(open_prices[-2]>open_sma_long_shift_1)\
                    and (open_prices[-1]>daily_poc_shift_0)and(open_prices[-2] > daily_poc_shift_1)\
                    and (abs(daily_poc_pct)>umbral_poc_pct)and(abs(daily_poc_pct_shift_1)>umbral_poc_pct)\
                    and (open_prices[-1]<current_short_high_sma)\
                    and (open_prices[-1]<current_long_high_sma)\

        hold_poc_sma = (open_prices[-1]>current_short_poc_sma)\
                and (open_prices[-1]>current_long_poc_sma)\

        #Sell        
        not_hold_long_short = (open_prices[-1]<current_short_low_sma)\
                and (open_prices[-2]<short_low_sma_shift_1)\
                and (open_prices_pct[-1] > umbral_open_pct)\
                and (current_open_sma_long > current_open_sma_short)\
                and (abs(daily_poc_pct) > umbral_poc_pct)and(abs(daily_poc_pct_shift_1) > umbral_poc_pct)\

        not_hold_low = (open_prices[-1]<current_long_poc)\
                and (open_prices_pct[-1]>umbral_open_pct)\

        
        hold = (hold_long_short and hold_poc_sma) or (hold_va_high and (current_bb_rate>self.threshold_bb_rate))
        not_hold =  not_hold_long_short or not_hold_low


        #
        # Cosas agregadas por Chun
        #

        recent_lows = candles_data.low.rolling(window=6).mean().shift().iloc[-1]
        factor_to_reduce_past_lows = 0.97
        past_lows = candles_data.low.shift(6).rolling(window=12).mean().shift().iloc[-1] * factor_to_reduce_past_lows
        #earning_hours = count_earning_hours(candles_data, 24)
        
        not_hold = not_hold or (recent_lows < past_lows)

        if hold:
            recommendation = 1
        elif not_hold:
            recommendation = 0
        else:
            recommendation = last_recommendation
        
        return recommendation, timestamp, self.calculate_prices_variation(open_prices)
        

 
    def get_variables(self,open_prices):
        variable = dict()
        variable['open_sma_short'] = open_prices[-self.n_candles_open_sma_short:].mean()
        variable['open_sma_long'] = open_prices[-self.n_candles_open_sma_long:].mean()
        variable['open_sma_short_shift_1'] = open_prices[-self.n_candles_open_sma_short-1:-1].mean()
        variable['open_sma_long_shift_1'] = open_prices[-self.n_candles_open_sma_long-1:-1].mean()
        bb_lband = bollinger_lband(open_prices, window=self.bb_periods, window_dev=self.bb_std_dev)[-1]
        bb_hband = bollinger_hband(open_prices, window=self.bb_periods, window_dev=self.bb_std_dev)[-1]
        variable['bb_hband'] = bb_hband
        variable['bb_rate'] = (bb_hband - bb_lband)/bb_lband

        return variable
    
    
    def increase_values_to_significant_magnitudes(self, candles_data, quote):
        """
        If quote_currency is BTC, the prices are too small, so we need to multiplier for 1e3 in order to avoid 
        numerical errors. 
        """
        if quote == 'BTC':
            numeric_columns = candles_data._get_numeric_data().columns
            candles_data[numeric_columns] = 10000*candles_data[numeric_columns]

        return candles_data 
    
    def get_candles_needed_for_running(self):
        return self.max_number_of_candles 


    def get_market_profile_for_a_slice(self, df, start_date, end_date):
        market_profile = MarketProfile(df)
        mp_slice = market_profile[start_date : end_date]
        return mp_slice.poc_price, mp_slice.value_area[0], mp_slice.value_area[1]


    def get_yesterday_market_profile(self, current_time, before_days):
        date_starting = datetime(current_time.year, current_time.month, current_time.day) - timedelta(days=before_days)
        return self.get_market_profile(5*12*24, date_starting) #minutos en una hora
        

    def get_market_profile(self, n_candles_vp, end_time, number_of_rolling=1):
        
        #Candles 5 min in order to calculate volume profile!! 
        time_frame_for_volume_profile = 5
        candles_for_volume_profile = self.prices_repository.get_candles(
                        self.base_currency, 
                        self.quote_currency, 
                        MINUTES_TO_BINANCE_INTERVAL[time_frame_for_volume_profile], 
                        n_candles_vp,
                        end_time)
        candles_for_volume_profile = self.increase_values_to_significant_magnitudes(candles_for_volume_profile, self.quote_currency)
        
        variable_vp = dict()
        df_vp = []
        df = pd.DataFrame()
        df['Close'] = candles_for_volume_profile.close
        df['Open'] = candles_for_volume_profile.open
        df['High'] = candles_for_volume_profile.high
        df['Low'] = candles_for_volume_profile.low
        df['Volume'] = candles_for_volume_profile.volume
        
        ###
        list_of_dates = [end_time - pd.Timedelta(i,'h') for i in range(number_of_rolling)]
        
        for date in list_of_dates:
            df_aux = df.loc[date-pd.Timedelta(n_candles_vp,'m') : date-pd.Timedelta(5,'m') ]

            start = df_aux.index[0]
            end = df_aux.index[-1]

            info = self.get_market_profile_for_a_slice(df_aux, start, end)
            df_vp.append({
                        'date' : date,
                        'poc' :info[0],
                        'va_low' :info[1],
                        'va_high' :info[2]})

        df_vp=pd.DataFrame(df_vp)    
        df_vp=df_vp.set_index('date')
        df_vp.index = pd.to_datetime(df_vp.index)
        df_vp.index = pd.DatetimeIndex([i.replace(tzinfo=None) for i in df_vp.index]) 

        variable_vp['poc'] = df_vp.mean().poc
        variable_vp['va_low'] = df_vp.mean().va_low
        variable_vp['va_high'] = df_vp.mean().va_high

        return variable_vp

    def count_earning_hours(self, original_df, n_candles, col_name='open'):
        df = original_df[-(n_candles):]    
        df['earning_hours_count_buy'] = np.nan
        for current_row in range(n_candles, len(df)+1):
            start = current_row - n_candles
            end = current_row
            df['earning_hours_count_buy'].iloc[end] = (df[col_name].iloc[start:end] < df[col_name].iloc[end]).sum()

        return df['earning_hours_count_buy'].iloc[-1]

