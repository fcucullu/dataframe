import collections
from os import execlpe
from sys import implementation
import plotly.graph_objs as go
import requests
from datetime import timedelta, datetime
import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime,timedelta
from sklearn import linear_model
from collections import defaultdict 
from sklearn.metrics import mean_squared_error

from recommendations.indicators.pivots_points import PivotPointProcessor
from recommendations.strategies.pair_strategies import Holding

from recommendations.strategies.candlestick import BinancePricesRepository, CandlestickRepository

class SupportAndResistanceProcessor_00():
    
    def __init__(self, short_pp_pct=0.01, long_pp_pct=0.05):

        self.short_pp_pct = short_pp_pct
        self.long_pp_pct = long_pp_pct
                
    def process_pivot_points(self, candles):
        ppp = PivotPointProcessor()
        local_max_and_min = ppp.filter_local_max_and_min(candles)
        short_pps = ppp.process_pivot_points_core(local_max_and_min.copy(), self.short_pp_pct)
        long_pps = ppp.process_pivot_points_core(local_max_and_min.copy(), self.long_pp_pct)
        
        self.candles = candles
        self.short_pps = short_pps
        self.long_pps = long_pps
        
        return candles, short_pps, long_pps

    
    def plot(self, supports_data=[],start_time=None, end_time=None):
        if start_time is None:
            start_time = self.candles.index[0]
            
        if end_time is None:
            end_time = self.candles.index[-1]
            
        candles = self.candles[start_time:end_time]
        short_pps = self.short_pps[start_time:end_time]
        long_pps = self.long_pps[start_time:end_time]
        
        try:
            tendency_start = self.get_tendency_start_date()
            print(f"short counter : {len(short_pps[tendency_start:])} - {tendency_start}")
        except:
            print("no tendency")
        
        candles_to_plot = go.Candlestick(
            x=candles.index, 
            open=candles.open, 
            high=candles.high, 
            low=candles.low, 
            close=candles.close)
        
        short_pps_to_plot = go.Scatter(
            x=short_pps.index,
            y=short_pps.pp,
            name='short_pps',
            #  mode='markers',
        )
        long_pps_to_plot = go.Scatter(
            x=long_pps.index,
            y=long_pps.pp,
            name='long_pps',
            #  mode='markers',
        )

        data=[candles_to_plot, long_pps_to_plot, short_pps_to_plot]
        
        i=0
        for model, x, y, mse in supports_data:
            data.append(go.Scatter(x=x, y=y, name=f"model_{i}, mse: {mse}"))
            i+=1
        
        
        annotations = []#self._get_annotation() 
        title = "Support and Resistance" #self._get_title()
        figSignal = go.Figure(data=data, layout={'title': title, 'annotations': annotations,  'height':600})
        figSignal.show()
        

    def create_linear_model(self, d, col_name):
        x = d['hours_from_tendency_start'].values.reshape(-1, 1)
        y = d[col_name].values
        model = linear_model.LinearRegression().fit(x, y)
        return model

    def get_tendency_start_date(self):
        limit = len(self.long_pps)
        i = 2
        while i <= limit:
            start_date = self.long_pps.index[-i]
            
            recent_pps = self.short_pps[start_date:]
            low_pp_n = len(recent_pps.low_pp[~recent_pps.low_pp.isna()])
            high_pp_n = len(recent_pps.high_pp[~recent_pps.high_pp.isna()])            
            
            if low_pp_n >= 2 and high_pp_n >= 2:
                return start_date
            i += 1
        return self.long_pps.index[0]

    def add_hours_from_tendency_start(self, df):         
        df.loc[:,'hours_from_tendency_start'] = (df.index - df.index[0]).total_seconds() / 3600
        return df

    def get_recent_pps(self):
        tendecy_start_date = self.get_tendency_start_date()
        recent_pps = self.short_pps[tendecy_start_date:].copy()
        recent_pps = self.add_hours_from_tendency_start(recent_pps)
        recent_pps['pp_position'] = [i for i in range(len(recent_pps))]
        return recent_pps

    def get_all_pp_pairs(self, recent_pps, col_name):
        """"Todos los posibles pares de pivots points para calcular el soporte/resistencia"""
        recent_pps = recent_pps[~recent_pps[col_name].isna()]
    
        n = len(recent_pps)
        all_pairs = [(i, j) for i in range(0, n) for j in range(i+1,n)]
    
        all_pp_pairs = [
            pd.DataFrame([recent_pps.iloc[i, :],recent_pps.iloc[j, :]])
            for i,j in all_pairs
        ]
    
        return all_pp_pairs
    
    def get_models_for_support_or_resistance(self, pp_type):
        recent_pps = self.get_recent_pps()
        all_pp_pairs = self.get_all_pp_pairs(recent_pps,  pp_type)
        my_pps = recent_pps[~recent_pps[pp_type].isna()]
        pp_x_axis_for_model = [[round(my_pps.hours_from_tendency_start.iloc[i], 2)] for i in range(len(my_pps))]
        
        models = defaultdict(list)
        for pair_pp in all_pp_pairs:
            m = self.create_linear_model(pair_pp, pp_type)
            support_or_resistance_values = m.predict(pp_x_axis_for_model)
            
            if pp_type == 'low_pp':
                values_outside_threshold_n = (~(my_pps.low_pp >= support_or_resistance_values * .999)).sum()
                mse = mean_squared_error(my_pps.low_pp, support_or_resistance_values)
                
            elif pp_type == 'high_pp':
                values_outside_threshold_n = (~(my_pps.high_pp * .999 < support_or_resistance_values)).sum()
                mse = mean_squared_error(my_pps.high_pp, support_or_resistance_values)
                
            models[values_outside_threshold_n].append((m, my_pps.index, support_or_resistance_values, mse))
    
        return models
    

    def function_to_select_model(self):
        get_mse = lambda x: x[3]
        get_slope = lambda x: x[0].coef_[0]

        return get_slope

    def process_support_and_resistance(self, candles):
        self.process_pivot_points(candles)
        
        support_candidates = self.get_models_for_support_or_resistance('low_pp')        
        self.support_model = None        
        if len(support_candidates) > 0:
            values_outside_threshold_n = min(support_candidates.keys())
            self.support_model, _, _, _ = min(
                support_candidates[values_outside_threshold_n], key=self.function_to_select_model())
        
        resistance_candidates = self.get_models_for_support_or_resistance('high_pp')
        self.resistance_model = None
        if len(resistance_candidates) > 0:
            values_outside_threshold_n = min(resistance_candidates.keys())
            self.resistance_model, _, _, _ = min(
                resistance_candidates[values_outside_threshold_n], key=self.function_to_select_model())
        
        self.set_last_tops()
        self.last_tops_model = self.create_linear_model(self.last_tops, 'high_pp')

        self.set_long_pps_tendency()

    def set_last_tops(self):
        tops = self.long_pps[~self.long_pps.high_pp.isna()]
        start_time = self.long_pps.index[-1] - timedelta(hours=24*7)

        last_tops = tops[start_time:]
        if len(last_tops) == 2:
            self.last_tops = last_tops[['high_pp']].copy()
        elif len(last_tops) < 2:
            self.last_tops = tops[-2:][['high_pp']].copy()            
        else:
            previous_tops = last_tops[:-1]
            self.last_tops = last_tops.loc[[previous_tops['high_pp'].idxmax(), last_tops.index[-1]]].copy()
            
        self.last_tops = self.add_hours_from_tendency_start(self.last_tops)

            
    def timestamp_as_hours_since_tendency_start(self, t):
        return (t - self.get_tendency_start_date()).total_seconds() / 3600
    
    def timestamp_as_hours_wrt_last_tops(self, t):
        return (t - self.last_tops.index[0]).total_seconds() / 3600    
    
    def plot_support_and_resistance(self):            
        candles = self.candles 
        short_pps = self.short_pps 
        long_pps = self.long_pps 
        
        try:
            tendency_start = self.get_tendency_start_date()
            print(f"short counter : {len(short_pps[tendency_start:])} - {tendency_start}")
        except:
            print("no tendency")
        
        candles_to_plot = go.Candlestick(
            x=candles.index, 
            open=candles.open, 
            high=candles.high, 
            low=candles.low, 
            close=candles.close)
        
        short_pps_to_plot = go.Scatter(
            x=short_pps.index,
            y=short_pps.pp,
            name='Short tendency / short_pps',
            #  mode='markers',
        )
        long_pps_to_plot = go.Scatter(
            x=long_pps.index,
            y=long_pps.pp,
            line=dict(color='Blue'),
            name='Main tendency / long_pps',
            #  mode='markers',
        )

        channel_timestamps = [
            self.get_tendency_start_date(),
            candles.index[-1]
        ]

        resistance = go.Scatter(
            x=channel_timestamps,
            y=self.resistance_model.predict([
                [self.timestamp_as_hours_since_tendency_start(t)]
                for t in channel_timestamps
            ]),
            name='resistance',            
        )

        support = go.Scatter(
            x=channel_timestamps,
            y=self.support_model.predict([
                [self.timestamp_as_hours_since_tendency_start(t)]
                for t in channel_timestamps
            ]),
            name='support',            
        )
        
        timestamps_for_tops_line = [
            self.last_tops.index[0],
            candles.index[-1],          
        ]
        
        tops_line = go.Scatter(
            x=timestamps_for_tops_line,
            y=self.last_tops_model.predict([
                [self.timestamp_as_hours_wrt_last_tops(t)]
                for t in timestamps_for_tops_line
            ]),
            name='tops line'            
        )
                
        data=[candles_to_plot, short_pps_to_plot, long_pps_to_plot, resistance, support, tops_line]        
        annotations = []
        title = f"Main tendency slope: {round(self.get_main_slope_value(), 4)} / Open price above resistance: {self.open_is_above_resistance()} "
        
        figSignal = go.Figure(data=data, layout={'title': title, 'annotations': annotations,  'height':600})
        figSignal.show()

    def get_main_slope_value(self):
        """La pendiente de la última linea que se calcula con los longs pivot points"""        
        return self.long_pps.pct[-1]
    
    def open_is_above_resistance(self):
        current_open = self.candles.open[-1]
        current_resistance = self.resistance_model.predict(
            [[self.timestamp_as_hours_since_tendency_start(self.candles.index[-1])]]
        )[0]
        return current_open >= current_resistance
    
    def set_long_pps_tendency(self, movement_pct=0.1):
        df = self.long_pps
        df['last_movement'] = np.nan
        df['tendency'] = np.nan
    
        df.loc[df.pct <= -movement_pct, ['tendency']] = 'bear'
        df.loc[df.pct >= movement_pct, ['tendency']]= 'bull'
        df['last_movement'] = df.tendency.copy()
    
        df.loc[(-movement_pct < df.pct) & (df.pct < movement_pct), ['tendency']] = 'lateral'
        df['last_movement'].mask(df['last_movement']=='nan', None).ffill(inplace=True)
    
        return df

    def is_bear_laretal_market(self):
        return (self.long_pps.last_movement[-1] == 'bear') and (self.long_pps.tendency[-1] == 'lateral')

    def is_bear_market(self):
        return (self.long_pps.last_movement[-1] == 'bear')


class ResistanceProcessor(SupportAndResistanceProcessor_00):

    def open_is_above_resistance(self):
        current_open = self.candles.open[-1]
        current_resistance = self.resistance_model.predict(
            [[self.timestamp_as_hours_since_last_bear_tendency_start(self.candles.index[-1])]]
        )[0]
        return current_open >= current_resistance

    def get_last_bear_tendency(self):
        df = self.short_pps.copy()
        df = df[:-1] # Eliminamos el último pp. Si es mínimo no calienta, si es máximo, no tiene que influir en la construción de la resistencia
        df = df[['high_pp']].dropna()
        df['change'] = df.high_pp.pct_change()
        df['last_bear_tendency'] = False
        
        # Buscando el final de la tendencia
        i = len(df) - 1
        while i > 0 and df.change.iloc[i] > 0:
            i -= 1

        # BOLUDEANDING: a veces es bueno sumar parte dell "final positivo" xq no se creció mucho.
        # Ejemplo: Link 31/8 a las 7am UTC
        #if i+1 < len(df) - 1:
        #    df.at[df.index[i+1], 'last_bear_tendency'] = True

        # desde la cola....
        while i > 0 and df.change.iloc[i] <= 0.015:
            df.at[df.index[i], 'last_bear_tendency'] = True
            i -= 1

        # hasta setear la cabeza
        if i > 0:
            df.at[df.index[i], 'last_bear_tendency'] = True
        
        return df

    def get_models_for_last_resistance(self):
        df = self.get_last_bear_tendency()
        recent_pps = df[df.last_bear_tendency].copy()
        recent_pps = self.add_hours_from_tendency_start(recent_pps)

        self.last_bear_tendency = recent_pps

        all_pp_pairs = self.get_all_pp_pairs(recent_pps,  'high_pp')
        my_pps = recent_pps
        pp_x_axis_for_model = [[round(my_pps.hours_from_tendency_start.iloc[i], 2)] for i in range(len(my_pps))]

        models = defaultdict(list)
        for pair_pp in all_pp_pairs:
            m = self.create_linear_model(pair_pp, 'high_pp')
            support_or_resistance_values = m.predict(pp_x_axis_for_model)
            
            values_outside_threshold_n = (~(my_pps.high_pp * .999 < support_or_resistance_values)).sum()
            mse = mean_squared_error(my_pps.high_pp, support_or_resistance_values)
            if m.coef_[0] < 0:
                models[values_outside_threshold_n].append((m, my_pps.index, support_or_resistance_values, mse))

        return models

    def timestamp_as_hours_since_last_bear_tendency_start(self, t):
        return (t - self.last_bear_tendency.index[0]).total_seconds() / 3600

    def function_to_select_model(self):
        get_mse = lambda x: x[3]
        get_slope = lambda x: x[0].coef_[0]

        return get_mse # vamos a minimizar usando mse xq entiendo que da resistencias con pocas caidas

    def process_last_resistance(self, candles):
        self.process_pivot_points(candles)
                
        resistance_candidates = self.get_models_for_last_resistance()
        self.resistance_model = None
        if len(resistance_candidates) > 0:
            values_outside_threshold_n = min(resistance_candidates.keys())
            self.resistance_model, _, _, _ = min(
                resistance_candidates[values_outside_threshold_n], key=self.function_to_select_model())

    def plot_resistance(self):            
        candles = self.candles 
        short_pps = self.short_pps 
        long_pps = self.long_pps 
                
        candles_to_plot = go.Candlestick(
            x=candles.index, 
            open=candles.open, 
            high=candles.high, 
            low=candles.low, 
            close=candles.close)
        
        short_pps_to_plot = go.Scatter(
            x=short_pps.index,
            y=short_pps.pp,
            name='Short tendency / short_pps',
            #  mode='markers',
        )

        long_pps_to_plot = go.Scatter(
            x=long_pps.index,
            y=long_pps.pp,
            line=dict(color='Blue'),
            name='Main tendency / long_pps',
            #  mode='markers',
        )

        channel_timestamps = [
            self.last_bear_tendency.index[0],
            candles.index[-1]
        ]

        color = "limegreen" if self.open_is_above_resistance() else 'red'
        resistance = go.Scatter(
            x=channel_timestamps,
            y=self.resistance_model.predict([
                [self.timestamp_as_hours_since_last_bear_tendency_start(t)]
                for t in channel_timestamps
            ]),
            line=dict(width=5, color=color),
            name='resistance',            
        )
                
        data=[candles_to_plot, short_pps_to_plot, long_pps_to_plot, resistance]        
        annotations = []
        title = f"Open price above resistance: {self.open_is_above_resistance()} "
        
        figSignal = go.Figure(data=data, layout={'title': title, 'annotations': annotations,  'height':600})
        figSignal.show()


class PivotPointStrategy_00(Holding):
    """
    Primera versión de la estrategia... fue algo a las apuradas... 
    La dejamos pq tiene cosas para re-usar
    """

    def get_candles_needed_for_running(self):
        """ Returns the numbers of candles needed to process the strategy """
        return 24 * 24

    def __init__(self, base_currency: str, quote_currency: str, time_frame: int=60, storage=None, 
        short_pp_pct=0.02, long_pp_pct=0.05, timeframe=2, prices_repository=BinancePricesRepository()):
        super().__init__(base_currency, quote_currency, time_frame, prices_repository)
        self.long_pp_pct = long_pp_pct 
        self.short_pp_pct = short_pp_pct
        self.historical_data = pd.DataFrame()
        self.timeframe = 2
        if storage is None:
            storage = {}
        self.storage = storage

    ## Run
    def run(self, last_recommendation: float=0, **kwargs):
        """ 
        return strategy status. Value 1 means hold, 0 means do not hold. 
        """
        candles = self.get_candles_data()
        timestamp = candles.index[-1]
        price_variation = self.calculate_prices_variation(candles['open'])

        recommendation = self.calculate_recommendation(candles)
        if recommendation is None:
            recommendation = last_recommendation

        return recommendation, timestamp, price_variation, self.storage

    def calculate_recommendation(self, original_candles):
        self.calculate_indicators_for_one_recommendations(original_candles)
        self.process_buy_signal()
        self.add_data_of_previous_run()
        self.process_trading_operations(initialize_columns=False) 
        self.update_storage()

        candles = self.historical_data
        current_state = candles['signal'].iloc[-1]
        previous_state = candles['signal'].iloc[-2]
        change_of_state =  current_state != previous_state
        recommendation = None
        if change_of_state and candles['signal'].iloc[-1] == self.LONG:
            # Solo se manda long (1) cuando se genera la señal sino no se manda nada 
            recommendation = 1
        elif candles['signal'].iloc[-1] == self.CLOSE_LONG:
            # Si la señal actual es close, siempre mando la señal. 
            recommendation = 0
        
        return recommendation

    def calculate_indicators_for_one_recommendations(self, candles):
        self.historical_data = pd.DataFrame()
        size = len(candles)
        for i in range(size-1, size+1):
            self.process_candles_iteration_for_historical_data(candles, i)

    def add_data_of_previous_run(self):
        key_and_def_values = [('buy', False), ('signal', 0), ('entry_price', np.nan), ('take_profit', False)]
        idx0 = self.historical_data.index[0]
        for k,v in key_and_def_values:
            previous_val = self.storage.get(k, v)
            if k in self.storage:
                if previous_val in ['True', 'False']:
                    previous_val = (previous_val == 'True')
                else:
                    previous_val = np.float64(previous_val)
            self.historical_data.at[idx0, k] = previous_val

    def update_storage(self):
        keys = ['buy', 'signal', 'entry_price', 'take_profit']
        idx = self.historical_data.index[-1]
        for k in keys:
            self.storage[k] = str(self.historical_data.at[idx, k])

    def run_over_a_df(self, candles, start_candle=24*9):
        self.historical_data = pd.DataFrame()

        for i in range(start_candle, len(candles)+1):
            self.process_candles_iteration_for_historical_data(candles, i)

        self.process_buy_signal()
        self.process_trading_operations()
        #self.process_returns()

        return self.historical_data

    def process_candles_iteration_for_historical_data(self, _raw_candles, n_candles=None):
        if n_candles is None:
            print(f"{len(_raw_candles)} to process")            
            raw_candles = _raw_candles            
        else:
            print(f"{n_candles}", end=", ")
            raw_candles = _raw_candles[:n_candles]
            if len(raw_candles) > self.get_candles_needed_for_running():
                raw_candles = raw_candles[- self.get_candles_needed_for_running():]
        
        sar_processor = SupportAndResistanceProcessor_00(long_pp_pct=self.long_pp_pct, short_pp_pct=self.short_pp_pct)
        sar_processor.process_support_and_resistance(self.clear_tail(raw_candles)) 

        channel_timestamps = [sar_processor.get_tendency_start_date(), raw_candles.index[-1]]        
        support_values = sar_processor.support_model.predict([
            [sar_processor.timestamp_as_hours_since_tendency_start(t)]
            for t in channel_timestamps])
        
        resistance_values = sar_processor.resistance_model.predict([
            [sar_processor.timestamp_as_hours_since_tendency_start(t)]
            for t in channel_timestamps])
        
        low_short_pps =  sar_processor.short_pps.low_pp.dropna()
        high_short_pps = sar_processor.short_pps.high_pp.dropna()
        
        timestamps_for_tops_line = [sar_processor.last_tops.index[0], raw_candles.index[-1]]
        tops_line_values = sar_processor.last_tops_model.predict([
            [sar_processor.timestamp_as_hours_wrt_last_tops(t)]
            for t in timestamps_for_tops_line
        ])
        
        current_data = pd.DataFrame(
            [{
                'n_candles': n_candles,
                'open': raw_candles.open[-1], 
                'resistance_slope': sar_processor.resistance_model.coef_[0], 
                'resistance': resistance_values[-1],
                'support_slope': sar_processor.support_model.coef_[0],
                'support': support_values[-1],
                'tops_line_slope': sar_processor.last_tops_model.coef_[0],
                'tops_line_start': tops_line_values[0],                    
                'tops_line': tops_line_values[-1],
                'ratio_support_resistance': support_values[-1]/resistance_values[-1],
                'last_low_short_pps': low_short_pps[-1],
                'last_high_short_pps': high_short_pps[-1],
                'is_bear': sar_processor.is_bear_market(),
                'is_bear_lateral': sar_processor.is_bear_laretal_market(),          
            }], 
            index = [raw_candles.index[-1]]
        )
        if n_candles is None:
            return current_data
        else:
            self.historical_data = pd.concat([self.historical_data, current_data])        
            return None

    def clear_tail(self, raw_candles):
        """ Los pivots points se calculan cada `self.timeframe` horas, entonces esto borra las velas extras.
        
        Si los pps se rocesan cada 6 hs, entonces a las 0, 6, 12 18 hs se deberian calcular los pps. 
        Notar que 
        """
        number_of_candles_in_the_current_frame = raw_candles.index[-1].hour % self.timeframe
        return raw_candles[:-(number_of_candles_in_the_current_frame + 1)].copy() ## CHEQUEAR ESTE +1  - Creo que está mal, pero lo dejo

    def process_buy_signal(self):
        falling = (self.historical_data.resistance_slope < 0) & (self.historical_data.support_slope < 0)
        self.historical_data['falling'] = falling
        open_gt_resistance = (self.historical_data.open > self.historical_data.resistance)
        open_gt_last_high_pp = (self.historical_data.open > self.historical_data.last_high_short_pps)

        flag = (self.historical_data.resistance_slope < 0) & (self.historical_data.support_slope > 0)
        close_flag = flag & (self.historical_data.ratio_support_resistance >= .95) & (self.historical_data.ratio_support_resistance <= 1.05)


        top_line_slope_is_positive = (self.historical_data.tops_line_slope > 0) & (self.historical_data.open > (self.historical_data.tops_line_start * .98))
        top_line_slope_is_negative = (self.historical_data.tops_line_slope <= 0) & (self.historical_data.open > self.historical_data.tops_line * 1.03)

        top_line_condition = top_line_slope_is_positive | top_line_slope_is_negative

        we_are_not_in_a_bear_market = ~self.historical_data.is_bear
        we_are_not_in_a_bear_lateral_market = ~self.historical_data.is_bear_lateral        

        self.historical_data['buy'] = we_are_not_in_a_bear_lateral_market & \
            top_line_condition & (
            (falling & open_gt_resistance & open_gt_last_high_pp)
            | (close_flag & open_gt_resistance & open_gt_last_high_pp)
        )

    def process_trading_operations(self, initialize_columns=True):
        if initialize_columns:
            self.historical_data['entry_price'] = None
            self.historical_data['signal'] = 0
            self.historical_data['close_trade_threshold'] = None
            self.historical_data['take_profit'] = None

        for i in range(1, len(self.historical_data)):
            idx = self.historical_data.index[i]    
            # BUY
            if self.historical_data['signal'][i-1] == 0 and self.historical_data.buy[i]:
                self.historical_data.at[idx, 'entry_price'] = self.historical_data.open.iloc[i]
                self.historical_data.at[idx, 'signal'] = 1
                self.historical_data.at[idx, 'take_profit'] = False
            # ALREADY BUY
            elif self.historical_data['signal'][i-1] == 1:
                entry_price = self.historical_data['entry_price'][i-1]
                self.historical_data.at[idx, 'entry_price'] = entry_price
                self.historical_data.at[idx, 'signal'] = 1
                self.historical_data.at[idx, 'take_profit'] = self.historical_data['take_profit'][i-1]      

                # check close trade
                # No tengo señal de compra, chequeo si tengo que salir 
                if self.historical_data.buy[i] == False:
                    support_slope_is_negative = self.historical_data.support_slope[i] < 0
                    if support_slope_is_negative and not self.historical_data['take_profit'][i]:
                        close_trade_threshold = entry_price * .98
                    else:
                        self.historical_data.at[idx, 'take_profit'] = True
                        a = self.historical_data.last_low_short_pps[i]
                        if self.historical_data.support_slope[i] > 0:
                            a = min(a,self.historical_data.support[i])
                        close_trade_threshold = max(entry_price * .98, a * .98)

                    resistance_below_support = (self.historical_data.resistance[i] < self.historical_data.support[i])
                    if self.historical_data.open[i] < close_trade_threshold or resistance_below_support:
                        self.historical_data.at[idx, 'signal'] = 0
                        self.historical_data.at[idx, 'entry_price'] = np.nan
                        self.historical_data.at[idx, 'take_profit'] = False
                    self.historical_data.at[idx, 'close_trade_threshold'] = close_trade_threshold
            else:
                self.historical_data.at[idx, 'signal'] = 0
                self.historical_data.at[idx, 'entry_price'] = np.nan
                self.historical_data.at[idx, 'take_profit'] = False
            

    def process_returns(self):
        self.historical_data['returns'] = self.historical_data.open.pct_change()
        self.historical_data['returns'].fillna(0, inplace=True)
        self.historical_data['strategy_returns'] = np.where(
            self.historical_data.signal.shift() == 1, 
            self.historical_data['returns'],
            0
        )
        p = self.historical_data.strategy_returns + 1 
        p[0] = self.historical_data.open[0]
        self.historical_data['strategy'] = p.cumprod()


class PivotPointStrategy(PivotPointStrategy_00):

    KEY_AND_DEFAULT_VALUES = [
            ('last_bear_max_value', 0),
            ('lower_low_1', np.nan), ('lower_low_2', np.nan), ('lower_low_3', np.nan), 
            ('lower_low_1_ts', np.nan), ('lower_low_2_ts', np.nan), ('lower_low_3_ts', np.nan),
            ('lower_low_counter', 0), ('max_lower_lows', 0), ('lower_lows', False),
            ('buy', False),  ('buy_signal_detected', False), ('already_one', False), ('entry_price', np.nan),
            ('sell', False), ('signal', 0),   
    ]

    def run_over_a_df(self, candles, start_candle=24*9):
        self.historical_data = pd.DataFrame()

        for i in range(start_candle, len(candles)+1):
            self.process_candles_iteration_for_historical_data(candles, i)

        return self.historical_data    

    def calculate_recommendation(self, original_candles):
        self.calculate_indicators_for_one_recommendations(original_candles)
        self.add_data_of_previous_run()
        self.process_signals_with_memory()
        self.process_buy_signal_with_memory()
        self.process_sell_signal()
        self.process_trading_operations(initialize_columns=False) 
        self.update_storage()

        candles = self.historical_data
        current_state = candles['signal'].iloc[-1]
        previous_state = candles['signal'].iloc[-2]
        change_of_state =  current_state != previous_state
        recommendation = None
        if change_of_state and candles['signal'].iloc[-1] == self.LONG:
            # Solo se manda long (1) cuando se genera la señal sino no se manda nada 
            recommendation = 1
        elif candles['signal'].iloc[-1] == self.CLOSE_LONG:
            # Si la señal actual es close, siempre mando la señal. 
            recommendation = 0
        
        return recommendation

    def add_data_of_previous_run(self):
        idx0 = self.historical_data.index[0]
        for k,v in self.KEY_AND_DEFAULT_VALUES:
            previous_val = self.storage.get(k, v)
            if k in self.storage:
                if previous_val in ['True', 'False']:
                    previous_val = (previous_val == 'True')
                elif k.endswith('_ts'):
                    try:
                        previous_val = pd.to_datetime(previous_val)
                    except Exception as e:
                        previous_val = np.nan
                else:
                    previous_val = np.float64(previous_val)
            self.historical_data.at[idx0, k] = previous_val
        
        self.historical_data.buy.fillna(False, inplace=True)
        self.historical_data.sell.fillna(False, inplace=True)

    def update_storage(self):
        keys = [ key_value[0] for key_value in self.KEY_AND_DEFAULT_VALUES]
        idx = self.historical_data.index[-1]
        for k in keys:
            self.storage[k] = str(self.historical_data.at[idx, k])  

    def process_candles_iteration_for_historical_data(self, _raw_candles, n_candles=None):
        if n_candles is None:
            print(f"{len(_raw_candles)} to process")            
            raw_candles = _raw_candles            
        else:
            print(f"{n_candles}", end=", ")
            raw_candles = _raw_candles[:n_candles]
            if len(raw_candles) > self.get_candles_needed_for_running():
                raw_candles = raw_candles[- self.get_candles_needed_for_running():]
        
        sar_processor = ResistanceProcessor(long_pp_pct=self.long_pp_pct, short_pp_pct=self.short_pp_pct)
        sar_processor.process_last_resistance(raw_candles) 
        sar_processor.set_long_pps_tendency()

        current_timestamp = raw_candles.index[-1]
        resistance_value = raw_candles.open[-1]
        model_coef = 0

        try:
            resistance_value = sar_processor.resistance_model.predict([
                [sar_processor.timestamp_as_hours_since_last_bear_tendency_start(current_timestamp)]
            ])[0]
            model_coef = sar_processor.resistance_model.coef_[0]
        except AttributeError:
            pass
                
        current_data = pd.DataFrame(
            [{
                'n_candles': n_candles,
                'open': raw_candles.open[-1],
                'high': raw_candles.high[-1],
                'low': raw_candles.low[-1],
                'close': raw_candles.close[-1],                
                'resistance_slope': model_coef, 
                'resistance': resistance_value,
                'last_short_high_pp': sar_processor.short_pps.high_pp.dropna()[-1],                
                'last_short_low_pp': sar_processor.short_pps.low_pp.dropna()[-1],
                'last_long_high_pp': sar_processor.long_pps.high_pp.dropna()[-1],                
                'last_long_low_pp': sar_processor.long_pps.low_pp.dropna()[-1],
                'last_movement' : sar_processor.long_pps.last_movement[-1], 
                'tendency': sar_processor.long_pps.tendency[-1], 
            }], 
            index = [raw_candles.index[-1]]
        )

        if n_candles is None:
            return current_data
        else:
            self.historical_data = pd.concat([self.historical_data, current_data])        
            return None

    def process_signals(self):
        self.process_last_bear_max_value()
        self.process_lower_lows()

    def process_last_bear_max_value(self):
        reset_highs = True
        current_highs = []

        self.historical_data['last_bear_max_value'] = 0
        for i in self.historical_data.index:    
            is_lateral_bear_market = self.historical_data.last_movement.loc[i] == 'bear' and self.historical_data.tendency.loc[i] == 'lateral'
            if is_lateral_bear_market:
                if reset_highs:
                    current_highs = []
                    reset_highs = False
                current_highs.append(self.historical_data.high.loc[i])
            else:
                reset_highs = True
                try:
                    max_val = np.nanmax(current_highs)
                except:
                    continue
                self.historical_data.loc[i, 'last_bear_max_value'] = max_val

    def process_signals_with_memory(self, last_row=None):
        if last_row is None:
            last_row = len(self.historical_data) - 1

        self.process_last_bear_max_value_with_memory(last_row)
        self.process_lower_lows_with_memory(last_row)


    def process_last_bear_max_value_with_memory(self, last_row):
        if 'last_bear_max_value' not in self.historical_data.columns:
            self.historical_data['last_bear_max_value'] = 0        

        i = last_row        
        current_index = self.historical_data.index[i]
        previous_index = self.historical_data.index[i-1]

        check_bear_lateral_market = lambda i: self.historical_data.last_movement.loc[i] == 'bear' and self.historical_data.tendency.loc[i] == 'lateral'
        is_lateral_bear_market = check_bear_lateral_market(current_index) 
        if is_lateral_bear_market:
            already_lateral_market = check_bear_lateral_market(previous_index)
            if already_lateral_market:
                self.historical_data.loc[current_index, 'last_bear_max_value'] = max(
                    self.historical_data.loc[previous_index, 'last_bear_max_value'], self.historical_data.high.loc[current_index]
                )
            else:
                self.historical_data.loc[current_index, 'last_bear_max_value'] = self.historical_data.high.loc[current_index]
        else: 
            self.historical_data.loc[current_index, 'last_bear_max_value'] = self.historical_data.loc[previous_index, 'last_bear_max_value']


    def process_lower_lows(self):
        n_for_rolling = 24 * 2
        max_lower_lows = lambda x: np.nanmax(x.drop_duplicates()[-3:])
        self.historical_data['max_lower_lows'] = self.historical_data.last_long_low_pp.rolling(n_for_rolling).apply(max_lower_lows)
        self.historical_data['max_lower_lows'].fillna(0, inplace=True)

        is_lower_lows = lambda x: x.drop_duplicates()[-3:].is_monotonic_decreasing and len(x.drop_duplicates()[-3:]) >= 2
        self.historical_data['lower_lows'] = self.historical_data.last_long_low_pp.rolling(n_for_rolling).apply(is_lower_lows)
        self.historical_data['lower_lows'].fillna(0, inplace=True)
        self.historical_data['lower_lows'] = self.historical_data['lower_lows'].apply(lambda x: x==1)
        self.historical_data['lower_lows'] &= (self.historical_data.open < self.historical_data.max_lower_lows)

    def process_lower_lows_with_memory(self, last_row=None):
        if last_row is not None and last_row > 0:
            self._copy_last_lower_lows_values(last_row=last_row)

        self.update_lower_lows(last_row=last_row)
        self.calculate_lower_lows(last_row=last_row)

    def _copy_last_lower_lows_values(self, last_row=None):
        if last_row is None:
            last_row = len(self.historical_data) - 1

        col_names = [
            'lower_low_1', 'lower_low_2', 'lower_low_3', 
            'lower_low_1_ts', 'lower_low_2_ts', 'lower_low_3_ts', 
            'lower_low_counter', 'max_lower_lows'
        ]

        for col_name in col_names:
            col_number = self.historical_data.columns.get_loc(col_name)
            if col_name.endswith('_ts'):
                try:
                    self.historical_data.iat[last_row, col_number] = self.historical_data.iat[last_row - 1, col_number]
                except ValueError as e:
                    print(e)
                    continue
            elif col_name.endswith('_counter'):
                self.historical_data.iat[last_row, col_number] = int(self.historical_data.iat[last_row - 1, col_number])
            else:
                self.historical_data.iat[last_row, col_number] = np.float64(self.historical_data.iat[last_row - 1, col_number])                

    def update_lower_lows(self, last_row=None):
        if last_row is None:
            last_row = len(self.historical_data) - 1
        
        if 'lower_low_1' not in self.historical_data.columns:
            self._create_lower_lows_cols()
        
        self._remove_old_lower_lows(last_row)

        counter = int(self.historical_data.lower_low_counter[last_row])
        no_lower_lows = counter == 0
        new_lower_low = False
        if not no_lower_lows:
            new_lower_low = self.historical_data[f'lower_low_{counter}'].iloc[last_row] != self.historical_data.last_long_low_pp.iloc[last_row]

        if no_lower_lows or new_lower_low:
            if counter == 3:
                self._remove_first_lower_lows(last_row)
            self._update_lower_lows_row(last_row)

        max_lower_lows_col_number = self.historical_data.columns.get_loc(f"max_lower_lows")

        self.historical_data.iat[last_row, max_lower_lows_col_number] = np.nanmax([
            self.historical_data[f'lower_low_{i}'].iloc[last_row] for i in [1,2,3]
        ])

    def calculate_lower_lows(self, last_row=None):
        if 'lower_lows' not in self.historical_data.columns:
            self.historical_data['lower_lows'] = False

        if last_row is None:
            last_row = len(self.historical_data) - 1

        highs_pivot_points = pd.Series([
            self.historical_data[f'lower_low_{i}'].iloc[last_row] for i in [1,2,3]
        ])
        lower_lows_col_number = self.historical_data.columns.get_loc(f"lower_lows")

        self.historical_data.iat[last_row, lower_lows_col_number] = (
            highs_pivot_points.dropna().is_monotonic_decreasing 
            and len(highs_pivot_points.dropna()) >= 2
            and self.historical_data.open.iloc[last_row] < self.historical_data.max_lower_lows.iloc[last_row])

    def _create_lower_lows_cols(self):
        # 1 es el más viejo, 3 el más reciente
        self.historical_data['lower_low_1'] = np.nan
        self.historical_data['lower_low_2'] = np.nan
        self.historical_data['lower_low_3'] = np.nan            
        self.historical_data['lower_low_1_ts'] = None
        self.historical_data['lower_low_2_ts'] = None
        self.historical_data['lower_low_3_ts'] = None
        self.historical_data['lower_low_counter'] = 0
        self.historical_data['max_lower_lows'] = np.nan

    def _remove_old_lower_lows(self, row_n):
        counter = int(self.historical_data.lower_low_counter[row_n])  
        if counter < 2:
            return

        oldest_ts = self.historical_data['lower_low_2_ts'].iloc[row_n] # el último se repite hasta que llega uno nuevo... pensalo.
        now = self.historical_data.index[row_n] # el "ahora" es con respecto al index actual
        if now - oldest_ts > timedelta(days=2):
            print(now, oldest_ts, now - oldest_ts)
            self._remove_first_lower_lows(row_n)

    def _remove_first_lower_lows(self, row_n):
        counter = int(self.historical_data.lower_low_counter[row_n])
        if counter == 0:
            return

        # muevo los valores mas recientes uno para adelante.
        for i in range(2,counter+1):
            lower_low_col_number = self.historical_data.columns.get_loc(f"lower_low_{i-1}")
            lower_low_ts_col_number = self.historical_data.columns.get_loc(f"lower_low_{i-1}_ts") 

            self.historical_data.iat[row_n, lower_low_col_number] = self.historical_data[f'lower_low_{i}'].iloc[row_n]
            self.historical_data.iat[row_n, lower_low_ts_col_number] = self.historical_data[f'lower_low_{i}_ts'].iloc[row_n]

        # libero el último espacio... 
        lower_low_col_number = self.historical_data.columns.get_loc(f"lower_low_{counter}")
        lower_low_ts_col_number = self.historical_data.columns.get_loc(f"lower_low_{counter}_ts") 
        self.historical_data.iat[row_n, lower_low_col_number] = None
        self.historical_data.iat[row_n, lower_low_ts_col_number] = None
        # ... y actualizo el counter
        counter_col_number = self.historical_data.columns.get_loc(f"lower_low_counter")
        self.historical_data.iat[row_n, counter_col_number] = counter-1        

    def _update_lower_lows_row(self, row_n):
        """Carga un nuevo lower low en la posicion 'counter + 1' (<=3) en base a los datos de la fila row_n"""

        counter = int(self.historical_data.lower_low_counter[row_n] + 1)
        
        counter_col_number = self.historical_data.columns.get_loc(f"lower_low_counter")
        lower_low_col_number = self.historical_data.columns.get_loc(f"lower_low_{counter}")
        lower_low_ts_col_number = self.historical_data.columns.get_loc(f"lower_low_{counter}_ts") 

        self.historical_data.iat[row_n, counter_col_number] = counter
        self.historical_data.iat[row_n, lower_low_col_number] = self.historical_data.last_long_low_pp.iloc[row_n]
        self.historical_data.iat[row_n, lower_low_ts_col_number] = self.historical_data.index[row_n]
        
        counter_col_number = self.historical_data.columns.get_loc(f"lower_low_counter")
        self.historical_data.iat[row_n, counter_col_number] = counter

    def process_buy_signal(self):
        self.historical_data['buy'] = False
        self.create_buy_signal_open_breaks_resistance()
        self.create_buy_signal_open_breaks_lbm_value()
        self.process_reentries_over_a_resistance()


    def process_buy_signal_with_memory(self, last_row=None):
        if 'buy' not in self.historical_data.columns:
            self.historical_data['buy'] = False

        if last_row is None:
            last_row = len(self.historical_data) - 1

        self.create_buy_signal_open_breaks_resistance_with_memory(last_row)
        self.update_memory_for_create_buy_signal_open_breaks_lbm_value(last_row)
        self.create_buy_signal_open_breaks_lbm_value_with_memory(last_row)
        self.process_reentries_over_a_resistance_with_memory(last_row)

    def create_buy_signal_open_breaks_resistance(self):
        now_above_resistance = (self.historical_data.open > self.historical_data.resistance)
        previous_state_below_resistance = (self.historical_data.open.shift().fillna(0) < self.historical_data.resistance)
        open_breaks_resistance = now_above_resistance & previous_state_below_resistance
        bull_market = self.historical_data.last_movement != 'bear'
        buy = open_breaks_resistance & bull_market
        self.historical_data.loc[buy, 'buy'] = True

    def create_buy_signal_open_breaks_resistance_with_memory(self, last_row):
        now_above_resistance = self.historical_data.open.iloc[last_row] > self.historical_data.resistance.iloc[last_row]
        previous_state_below_resistance = self.historical_data.open.iloc[last_row-1] < self.historical_data.resistance.iloc[last_row]
        open_breaks_resistance = now_above_resistance & previous_state_below_resistance
        bull_market = self.historical_data.last_movement.iloc[last_row] != 'bear'
        buy = open_breaks_resistance & bull_market
        if buy:
            buy_col_number = self.historical_data.columns.get_loc("buy")            
            self.historical_data.iat[last_row, buy_col_number] = True


    def create_buy_signal_open_breaks_lbm_value(self):
        above_resistance = self.historical_data.open > self.historical_data.resistance
        now_above_last_bear_max_value = (self.historical_data.open > self.historical_data.last_bear_max_value)
        previous_state_below_last_bear_max_value = (self.historical_data.open.shift().fillna(0) < self.historical_data.last_bear_max_value)
        self.historical_data['open_breaks_lbm_value'] = above_resistance & now_above_last_bear_max_value & previous_state_below_last_bear_max_value

        buy_signal_detected = False
        buy_col_number = self.historical_data.columns.get_loc("buy")

        if 'buy_signal_detected' not in self.historical_data.columns: 
            self.historical_data['buy_signal_detected'] = False
        buy_signal_detected_col = self.historical_data.columns.get_loc("buy_signal_detected")   

        for i in range(1,len(self.historical_data)):
            new_resistance = self.historical_data.resistance_slope[i-1] != self.historical_data.resistance_slope[i]
            if new_resistance:
                buy_signal_detected = False
                
            if buy_signal_detected:
                if self.historical_data.open_breaks_lbm_value[i]:
                    self.historical_data.iat[i, buy_col_number] = True            
            elif self.historical_data.buy[i]:
                buy_signal_detected = True

            self.historical_data.iat[i, buy_signal_detected_col] = buy_signal_detected

    def update_memory_for_create_buy_signal_open_breaks_lbm_value(self, last_row=None):
        if last_row is None:
            last_row = len(self.historical_data) -1

        if 'buy_signal_detected' not in self.historical_data.columns: 
            self.historical_data['buy_signal_detected'] = False
        
        buy_signal_detected_col = self.historical_data.columns.get_loc("buy_signal_detected")        

        new_resistance = False
        if last_row > 0:
            # chequeo si hay nueva resistencia
            new_resistance = self.historical_data.resistance_slope[last_row] != self.historical_data.resistance_slope[last_row-1]
            # copio el valor anterior al actual:
            self.historical_data.iat[last_row, buy_signal_detected_col] = self.historical_data.iat[last_row-1, buy_signal_detected_col]
        
        if new_resistance:
            # si la resistencia es nueva, el valor anterior me ne frega
            self.historical_data.iat[last_row, buy_signal_detected_col] = False
        
        if self.historical_data.buy[last_row] == True:
            self.historical_data.iat[last_row, buy_signal_detected_col] = True 


    def create_buy_signal_open_breaks_lbm_value_with_memory(self, last_row):
        above_resistance = self.historical_data.open.iloc[last_row] > self.historical_data.resistance.iloc[last_row]
        now_above_last_bear_max_value = (self.historical_data.open.iloc[last_row] > self.historical_data.last_bear_max_value.iloc[last_row])
        previous_state_below_last_bear_max_value = (self.historical_data.open.iloc[last_row-1] < self.historical_data.last_bear_max_value.iloc[last_row])
        open_breaks_lbm_value = above_resistance & now_above_last_bear_max_value & previous_state_below_last_bear_max_value

        buy_col_number = self.historical_data.columns.get_loc("buy")
        if self.historical_data.buy_signal_detected[last_row]:
            if open_breaks_lbm_value:
                self.historical_data.iat[last_row, buy_col_number] = True 

    def process_reentries_over_a_resistance(self):
        already_one = False
        buy_col_number = self.historical_data.columns.get_loc("buy")
        for i in range(1,len(self.historical_data)):
            new_resistance = self.historical_data.resistance_slope[i-1] != self.historical_data.resistance_slope[i]            
            if new_resistance:
                already_one = False
                
            if already_one:
                price_above_last_entry = (self.historical_data.open[i] > entry_price)
                open_above_resistance = self.historical_data.open[i] > self.historical_data.resistance[i]
                if price_above_last_entry and open_above_resistance:
                    self.historical_data.iat[i, buy_col_number] = True
                    #entry_price = self.historical_data.open[i]                    
                else:
                    self.historical_data.iat[i, buy_col_number] = False
                
            elif self.historical_data.buy[i] == True:
                already_one = True
                entry_price = self.historical_data.open[i] 

    def process_reentries_over_a_resistance_with_memory(self, last_row):
        if 'already_one' not in self.historical_data.columns: 
            self.historical_data['already_one'] = False
            self.historical_data['entry_price'] = np.nan

        already_one_col_number = self.historical_data.columns.get_loc("already_one")
        entry_price_col_number = self.historical_data.columns.get_loc("entry_price")
        buy_col_number = self.historical_data.columns.get_loc("buy")

        self.historical_data.iat[last_row, already_one_col_number] = self.historical_data.iat[last_row-1, already_one_col_number]
        self.historical_data.iat[last_row, entry_price_col_number] = self.historical_data.iat[last_row-1, entry_price_col_number]             

        new_resistance = self.historical_data.resistance_slope[last_row-1] != self.historical_data.resistance_slope[last_row]            
        if new_resistance:
            self.historical_data.iat[last_row, already_one_col_number]  = False

        if self.historical_data.already_one[last_row] == True:
            price_above_last_entry = (self.historical_data.open[last_row] > self.historical_data.entry_price[last_row])
            open_above_resistance = self.historical_data.open[last_row] > self.historical_data.resistance[last_row]
            if price_above_last_entry and open_above_resistance:
                self.historical_data.iat[last_row, buy_col_number] = True
                #entry_price = self.historical_data.open[i]                    
            else:
                self.historical_data.iat[last_row, buy_col_number] = False
        elif self.historical_data.buy[last_row] == True:
            self.historical_data.iat[last_row, already_one_col_number] = True
            self.historical_data.iat[last_row, entry_price_col_number] = self.historical_data.open[last_row]

    def process_sell_signal(self):
        self.historical_data['sell'] = False
        self.create_sell_signal_below_resistance()
        self.create_sell_signal_bear_market_starts()
        self.create_sell_signal_lower_lows()
        self.create_sell_signal_below_last_bear_max()


    def create_sell_signal_lower_lows(self):
        self.historical_data.sell |= self.historical_data.lower_lows 

    def create_sell_signal_below_last_bear_max(self):
        self.historical_data.sell |= self.historical_data.open < self.historical_data.last_bear_max_value

    def create_sell_signal_below_resistance(self):
        price_below_resistance = self.historical_data.open <= self.historical_data.resistance
        self.historical_data.loc[price_below_resistance, 'sell'] = True

    def create_sell_signal_bear_market_starts(self):
        bear_market_start = (self.historical_data.last_movement == 'bear') & (self.historical_data.last_movement.shift() != 'bear').fillna(True)
        self.historical_data.loc[bear_market_start, 'sell'] = True

    def process_trading_operations(self, initialize_columns=True):
        if initialize_columns:
            self.historical_data['signal'] = None

        self.historical_data.loc[self.historical_data.buy, 'signal'] = 1
        self.historical_data.loc[self.historical_data.sell, 'signal'] = 0
        self.historical_data.signal.ffill(inplace=True)
        self.historical_data.signal.fillna(0, inplace=True)   

    def plot(self):
        self.historical_data[['open', 'strategy', 'resistance', 'last_low_pp']].plot(figsize=(18,8))
