import requests
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
from ta.volatility import average_true_range
from recommendations.strategies.candlestick import CandlestickRepository,BinancePricesRepository
from save_data.code.influx_manager import InfluxManager
from django.conf import settings

class Dominance():
    MARKET_CAP_COINDEX_URL = 'https://coincodex.com/apps/coincodex/cache/all_coins.json?market_cap_rank'
    def __init__(self,
                 alt_coins=['ADA','DOT','XRP','BNB','LTC','LINK','DOGE','BCH','XLM','AAVE','UNI','EOS',
                            'XMR','MKR','SNX','COMP'],
                 stable_coins=['USDT','DAI',"USDC"],
                 periods = 1,
                 ):
        """
        Por defecto la construcción de la dominancia tiene en cuenta BTC y ETH. El resto de las monedas se las debe clasificar en dos categorías:
            - alt_coins
            - stable coins

        Parameters
        ----------
        alt_coins : list, optional
            lista con las alt coins a tener en cuenta. The default is ['ADA','DOT','XRP','BNB','LTC','LINK','DOGE','BCH','XLM','AAVE','UNI','EOS','XMR','MKR','SNX','COMP',"WBTC"].
        stable_coins : list, optional
            lista con las stable coins a tener en cuenta. The default is ['USDT','DAI',"USDC"].
        BTC y ETH no se consideran como alt_coins
        Returns
        -------
        None.

        """
        self.alt_coins = alt_coins
        self.stable_coins = stable_coins
        self.all_coins = ["BTC","ETH"] + self.alt_coins + self.stable_coins
        self.periods = periods
        self.repo = BinancePricesRepository()
        
    def built_coin_history_url(self,coin,start_date,end_date):
        coin_history_url = f'https://coincodex.com/api/coincodex/get_coin_history/{coin}/{start_date}/{end_date}/50000'
        return coin_history_url
    
    def get_historical_dominances(self,start_date = datetime(2020,1,1), end_date = datetime.now()+timedelta(days=1)):
        coins_in_circulation = pd.DataFrame()
        market_cap = pd.DataFrame()
        start_date = start_date.strftime('%Y-%m-%d')
        end_date = end_date.strftime('%Y-%m-%d')
        for coin in self.all_coins:
            coin_history_url = self.built_coin_history_url(coin,start_date,end_date)
            data = requests.get(coin_history_url)
            data = data.json()
            if len(data)==0:
                continue
            df = pd.DataFrame(data[coin])
            df.columns = ['date','price','volume','market_cap']
            df.date = df.date.apply(lambda x : datetime.fromtimestamp(x))
            df = df.drop_duplicates()
            df["coins_in_circulation"] = df['market_cap']/df['price']
            df = df.set_index('date')
            coins_in_circulation[coin] = df["coins_in_circulation"] 
            market_cap[coin] = df["market_cap"]
        self.historical_dominances = market_cap.multiply(1/market_cap.sum(axis=1),axis=0)
        self.historical_dominances["stable_coins"] = self.historical_dominances[self.stable_coins].sum(axis=1)
        self.historical_dominances["alt_coins"] = self.historical_dominances[self.alt_coins].sum(axis=1)
        self.historical_coins_in_circulation = coins_in_circulation
        return self.historical_dominances
    
    def get_historical_built_dominances(self,prices):
        """
        Función para construir la dominancia a partir de los precios de Binance, 
        esto también tiene la ventaja de poder usar la misma temporalidad que las velas que se ingresan.
        Para hacer eso se aproxima la capitalización de mercado con el precio del momento multiplicado por
        la última información que se tiene sobre las monedas en circulación (se espera que no cambie mucho).

        Parameters
        ----------
        prices : DataFrame
            DataFrame con todos los precios de BTC, ETH, altcoins y stable coins.
            
        Returns
        -------
        DataFrame
            DataFrame con la dominancia historica reconstruída con Dataframe precios.
        """
        prices = prices.copy()
        try:
            self.historical_coins_in_circulation
        except AttributeError:
            self.get_historical_dominances(prices.index[0],prices.index[-1])
        market_cap = self.historical_coins_in_circulation.copy()
        new_index = market_cap.index.append(prices.index).drop_duplicates()
        market_cap = market_cap.reindex(index=new_index).sort_index().ffill().multiply(prices).dropna()
        market_cap = market_cap[market_cap.index>=prices.index[0]]
        market_cap = market_cap[market_cap.index<=prices.index[-1]]
        market_cap["stable_coins"] = market_cap[self.stable_coins].sum(axis=1)
        market_cap["alt_coins"] = market_cap[self.alt_coins].sum(axis=1)
        self.historical_built_dominances = market_cap.multiply(1/market_cap.sum(axis=1),axis=0)
        return self.historical_built_dominances
    
    def read_prices(self):
        end_date = datetime.now()
        quote = "USDT"
        prices_1h = pd.DataFrame()
        prices_15m = pd.DataFrame()
        prices_5m = pd.DataFrame()
        prices_high = pd.DataFrame()
        prices_low = pd.DataFrame()
        self.all_coins.remove(quote)
        self.all_coins = self.all_coins + [quote] 
        
        for base in self.all_coins:
            if base == quote:
                    prices_5m[base] = 1.
                    prices_15m[base] = 1.
                    prices_1h[base] = 1.
                    prices_high[base] = 1.
                    prices_low[base] = 1.
                    continue
            price = self.repo.get_candles(base,quote,'1h',23+self.periods,end_date)
            if price.empty:
                continue
            prices_1h[base] = price['open']
            prices_high[base] = price['high'].shift(1)
            prices_low[base] = price['low'].shift(1)
            price = self.repo.get_candles(base,quote,'5m',113+12*self.periods,end_date)
            prices_5m[base] = price['open']
            price = self.repo.get_candles(base,quote,'15m',105+4*self.periods,end_date)
            prices_15m[base] = price['open']
        
        # prices_1h.index = [i.astimezone(None) for i in prices_1h.index]
        # prices_5m.index = [i.astimezone(None) for i in prices_5m.index]
        # prices_15m.index = [i.astimezone(None) for i in prices_15m.index]
        # prices_high.index = [i.astimezone(None) for i in prices_high.index]
        # prices_low.index = [i.astimezone(None) for i in prices_low.index]
        
        return prices_5m,prices_15m,prices_1h,prices_high,prices_low
     
    def set_dominance(self,from_db=True):
        if from_db:
            
            influxmanager = InfluxManager(settings.INFLUX_DATABASE_BC)
            start_date = datetime.utcnow()-timedelta(minutes=self.periods*5)
            df = influxmanager.get_data(date_from = start_date,date_until=datetime.utcnow())
            df = df.reindex(pd.date_range(start=df.index[0],end=df.index[-1],freq='5t'))
            df = df.ffill()
            df.loc[df['BTC_dominance'].pct_change()<-0.9,'BTC_dominance'] = np.nan
            df['BTC_dominance'] = df['BTC_dominance'].ffill()
            dominances_5m,dominances_15m,dominances_1h,dominances_high,dominances_low =\
                (pd.DataFrame() for x in range(5))
            dominances_5m['BTC'] = df['BTC_dominance']
            time_grouper = pd.Grouper(freq="1h")
            dominances_high['BTC'] = df.groupby(time_grouper)['BTC_dominance'].max()
            dominances_low['BTC'] = df.groupby(time_grouper)['BTC_dominance'].min()
            dominances_1h['BTC'] = df.groupby(time_grouper)['BTC_dominance'].last()
            time_grouper = pd.Grouper(freq="15t")
            dominances_15m['BTC'] = df.groupby(time_grouper)['BTC_dominance'].last()
            return dominances_5m,dominances_15m,dominances_1h,dominances_high,dominances_low

        prices_5m,prices_15m,prices_1h,prices_high,prices_low = self.read_prices()
        dominances_1h = self.get_historical_built_dominances(prices_1h.ffill())
        dominances_high = self.get_historical_built_dominances(prices_high.ffill())
        dominances_low = self.get_historical_built_dominances(prices_low.ffill())
        dominances_5m = self.get_historical_built_dominances(prices_5m.ffill())
        dominances_15m = self.get_historical_built_dominances(prices_15m.ffill())
        return dominances_5m,dominances_15m,dominances_1h,dominances_high,dominances_low
    
    
    def get_btc_dominance_increasing_indicators(self):
        dominances_5m,dominances_15m,dominances_1h,dominances_high,dominances_low = self.set_dominance()
        indicators = pd.DataFrame(index=dominances_1h.index)
        indicators["5m_sma"] = 0
        indicators["5m_sma"] = dominances_5m['BTC'].rolling(100).mean()[dominances_5m.index.minute==0]
        indicators["15m_sma"] = 0
        indicators["15m_sma"] = dominances_15m['BTC'].rolling(100).mean()[dominances_15m.index.minute==0]
        indicators["sma_short"] = dominances_1h['BTC'].rolling(8).mean()
        indicators["last_returns"] = (1+dominances_1h['BTC'].pct_change()).rolling(21).apply(np.prod)-1
        indicators["ATR_dir"] = np.where(indicators["last_returns"]>0,-1,1)
        indicators["ATR"] = average_true_range(high=dominances_high['BTC'],low=dominances_low['BTC'],close=dominances_1h['BTC'],window=13)
        indicators["sma_long"] = (dominances_1h['BTC']+indicators["ATR_dir"]*indicators["ATR"]).rolling(13).mean()
        indicators["dominances_1h"] = dominances_1h["BTC"]
        indicators["signal"] = (indicators['sma_long']<indicators['sma_short']) &\
            (dominances_1h["BTC"]>indicators['5m_sma']) &\
            (dominances_1h["BTC"]>indicators['15m_sma'])
        return indicators
    
    def get_btc_dominance_increasing_signal(self):
        indicators= self.get_btc_dominance_increasing_indicators()
        return indicators["signal"].iloc[-1]
        
        