import pandas as pd
import numpy as np
from datetime import datetime, timedelta
from decimal import Decimal as D
from ta.momentum import rsi    
from recommendations.strategies.candlestick import BinancePricesRepository, MINUTES_TO_BINANCE_INTERVAL
from recommendations.strategies.pair_strategies import Holding


class OrderBlockStrategy(Holding):

    MAXIMUM_LOSS = 5 
    MAX_NUMBER_OF_CANDLES = 4*24*50 #
    MAXIMUM_NUMBER_OF_DAYS_IN_A_TRADE = 4  
    TDI_RANGE = 4.5
    SMA_RANGE = 4.5
    N_LSB = 2

    ## Init
    def __init__(self, 
        base_currency: str, 
        quote_currency: str,
        time_frame: int, 
        type_OB: str,
        rr_ratio: float,
        n_rolling_sma_trend: int, 
        n_order_blocks: int,
        minimum_percent_of_change_to_detect_ob: float,  
        storage: dict,
        check_trend: bool,
        prices_repository=BinancePricesRepository()):

        """ 
        type_OB: bullish / bearish
        rr_ratio: the risk reward ratio
        n_rolling_sma_trend: number of candles to calculate sma, in case we check the tendency with a sma 
        n_order_blocks: number of candles to detect an order block 
        storage: a dictionary to save some information if there is a open position
        check_trend: True / False depend on you want to ckeck the tendency with a sma
        """

        super().__init__(base_currency, quote_currency, time_frame, prices_repository)         
        self.type_OB = type_OB
        self.rr_ratio = rr_ratio
        self.n_rolling_sma_trend = int(n_rolling_sma_trend)
        self.n_order_blocks = int(n_order_blocks)
        self.minimum_percent_of_change_to_detect_ob = minimum_percent_of_change_to_detect_ob  
        self.storage = storage
        self.check_trend = check_trend 

    ## Run
    def run(self, last_recommendation: float =0, **kwargs):
        """ 
        return strategy status. Value 1 means hold, 0 means do not hold. 
        """
        
        candles_data = self.get_candles_data()
        timestamp = candles_data.index[-1]

        self.date_open_position = self.storage.get('date_open_position', timestamp.timestamp())
        self.date_open_position = datetime.fromtimestamp(self.date_open_position) 

        price_variation = self.calculate_prices_variation(candles_data['open'])
        recommendation = self.calculate_recommendation(candles_data, last_recommendation)

        return recommendation, timestamp, price_variation, self.storage

    ## Calculate Reco  
    def calculate_recommendation(self, candles, last_recommendation): 
                    
        recommendation = last_recommendation
        if last_recommendation:
            if self.check_conditions_to_close_a_position(candles):
                recommendation = 0
        else:        
            if self.check_conditions_to_open_a_position(candles): 
                self.storage['date_open_position'] = datetime.timestamp(candles.index[-1].to_pydatetime())

                self.calculate_tp_and_sl(candles)
                recommendation = 1
        
        return recommendation
    
    ## Open trade
    def check_conditions_to_open_a_position(self, candles_data):

        timestamp = candles_data.index[-1]       
      
        if (timestamp.hour % 4 != 0) | (timestamp.minute != 0):
            return False

        ### Aquí velas de 4 hs!
        candles_data_ob = self.get_candles_data_to_detect_OB()
        candles_data_ob = candles_data_ob.iloc[:-1] 
        close_prices_ob = candles_data_ob['close']
        close_prices_pct_ob = close_prices_ob.pct_change()   
        
        if not self.get_OB(candles_data_ob, close_prices_pct_ob):
            return False    
        if not self.check_conditions_for_OB(candles_data_ob, self.TDI_RANGE, self.SMA_RANGE):
            return False

        return True
           
    ## Close trade
    def check_conditions_to_close_a_position(self, candles_data):
    
        df = candles_data.loc[self.date_open_position:] 
 
        if len(df) >= 4*24*self.MAXIMUM_NUMBER_OF_DAYS_IN_A_TRADE:
            return True
        
        loss_reward = (df['open'].iloc[-1] - df['open'].iloc[0])/df['open'].iloc[0]
        loss_reward = 100 * loss_reward

        if self.type_OB == 'bullish':
            tp_condition = loss_reward >= self.storage['take_profit']
            sl_condition = df['open'].iloc[-1] <= self.storage['stop_loss']
            max_loss_condition = loss_reward <= -self.MAXIMUM_LOSS

            return tp_condition | sl_condition | max_loss_condition
    
        if self.type_OB == 'bearish':
            tp_condition = loss_reward <= self.storage['take_profit']
            sl_condition = df['open'].iloc[-1] >= self.storage['stop_loss']
            max_loss_condition = loss_reward >= self.MAXIMUM_LOSS

            return tp_condition | sl_condition | max_loss_condition
    
    ## Take Profit and Stop Loss
    def calculate_tp_and_sl(self, candles_data):
                
        if self.type_OB == 'bullish':     
            stop_loss = self.storage['OB_low']
            entry =  self.storage['OB_high']

        if self.type_OB == 'bearish':
            stop_loss = self.storage['OB_high']
            entry =  self.storage['OB_low']

        take_profit = 100 * ((((self.rr_ratio + 1) * entry - self.rr_ratio * stop_loss)/entry) -1)

        self.storage['take_profit'] = take_profit
        self.storage['stop_loss'] = stop_loss
        
    
    ## Checking conditions
    def check_conditions_for_OB(self, candles_data, tdi_range, sma_range):
        
        if self.get_OB_validation_sma(candles_data): 
            
            if self. get_validation_tdi(candles_data):
                OB_low, OB_high = self.get_OB_levels(candles_data)
                
                if self.get_validations_OB(candles_data, self.N_LSB, OB_low, OB_high):
                    self.storage = { 
                                    'OB_low' : OB_low,
                                    'OB_high' : OB_high  
                                    }
                    return True

        return False
    
    ## Validated  OB
    def get_validations_OB(self, candles, N_LSB, OB_low, OB_high):

        df_lsb = candles.iloc[ -self.n_order_blocks : ]
 
        if self.type_OB == 'bullish':
            ob_is_valid = len(df_lsb[df_lsb['low'] <= OB_low]) > 0
            if ob_is_valid and self.check_trend:
                first_date_of_lsb = df_lsb[df_lsb['low'] <= OB_low].index[0]
                ob_is_valid = ob_is_valid and self.get_signal_sma_trend(candles, self.n_rolling_sma_trend, first_date_of_lsb)
            return ob_is_valid

        if self.type_OB == 'bearish':
            ob_is_valid = len(df_lsb[df_lsb['high'] >= OB_high]) > 0
            if ob_is_valid and self.check_trend:
                first_date_of_lsb = df_lsb[df_lsb['high'] >= OB_high].index[0]
                ob_is_valid = ob_is_valid and self.get_signal_sma_trend(candles, self.n_rolling_sma_trend, first_date_of_lsb)
            return ob_is_valid

    ##
    def get_signal_sma_trend(self, df, n_rolling_sma, date_of_lsb):

        df['sma_trend'] = df['open'].rolling(window=n_rolling_sma).mean()

        if self.type_OB == 'bullish':
            return df.loc[date_of_lsb, 'sma_trend'] < df.loc[date_of_lsb, 'low']

        if self.type_OB == 'bearish':
            return df.loc[date_of_lsb, 'sma_trend'] > df.loc[date_of_lsb,  'low']
            
    ##
    def get_OB_levels(self, candles):

        OB_high = candles['high'].iloc[-(self.n_order_blocks+1)]
        OB_low = candles['low'].iloc[-(self.n_order_blocks+1)]

        return OB_low, OB_high
    
    ##   
    def get_OB(self, prices,  prices_pct):

        if self.type_OB == 'bullish':
            return self.get_OB_bullish(prices, prices_pct)
        if self.type_OB == 'bearish':
            return self.get_OB_bearish(prices, prices_pct)

    ##
    # def get_OB_bullish(self, prices, prices_pct):
        
    #     if not ((prices_pct[-self.n_order_blocks:] >0).sum() == self.n_order_blocks) and (prices_pct[-(self.n_order_blocks+1)] < 0):
    #         return False

    #     prices_pct_ob = prices_pct[-self.n_order_blocks-1:] 
    #     prices_ob = prices[-self.n_order_blocks-1:] 
    #     abs_move = abs((prices_ob.close.iloc[-1] - prices_ob.close.iloc[0])/prices_ob.close.iloc[0]) 
    #     abs_move = 100 * abs_move     
    #     print('%', abs_move, prices_ob.close.iloc[-1],prices_ob.close.iloc[0])
    #     return abs_move >= self.minimum_percent_of_change_to_detect_ob
    
    def get_OB_bullish(self, prices, prices_pct):
        
        cond1 = (prices_pct[-self.n_order_blocks:] <0).sum() == 0 
        cond2 = prices_pct[-self.n_order_blocks-1] < 0 
        
        if cond1 and cond2:
            prices_pct_ob = prices_pct[-self.n_order_blocks-1:] 
            prices_ob = prices[-self.n_order_blocks-1:] 
            abs_move = abs((prices_ob.close.iloc[-1] - prices_ob.close.iloc[0])/prices_ob.close.iloc[0]) 
            abs_move = 100 * abs_move     
       
            return abs_move >= self.minimum_percent_of_change_to_detect_ob
        else:
            return False

    ##
    def get_OB_bearish(self, prices, prices_pct): 

        cond1 = (prices_pct[-self.n_order_blocks:] >0).sum() == 0 
        cond2 = prices_pct[-self.n_order_blocks-1] > 0 

        if cond1 and cond2:  
            prices_pct_ob = prices_pct[-self.n_order_blocks:]
            prices_ob = prices[-self.n_order_blocks:] #
            abs_move = abs((prices_ob.close.iloc[-1] - prices_ob.close.iloc[0])/prices_ob.close.iloc[0]) 
            abs_move = 100 * abs_move     

            return abs_move >= self.minimum_percent_of_change_to_detect_ob
        else:
            return False   
    ##         
    def get_OB_validation_sma(self, candles_data):
        
        ema13 = candles_data['close'].ewm(span = 13, adjust = False).mean() 
        ema200 = candles_data['close'].ewm(span = 200, adjust = False).mean() 

        ema13_less_than_high = ema13[-(self.n_order_blocks+1)] < candles_data['high'].iloc[-(self.n_order_blocks+1)]
        ema13_greater_than_low = ema13[-(self.n_order_blocks+1)] > candles_data['low'].iloc[-(self.n_order_blocks+1)]

        ema200_less_than_high = ema200[-(self.n_order_blocks+1)] < candles_data['high'].iloc[-(self.n_order_blocks+1)]
        ema200_greater_than_low = ema200[-(self.n_order_blocks+1)] > candles_data['low'].iloc[-(self.n_order_blocks+1)]
        
        return ((ema13_less_than_high and ema13_greater_than_low) or (ema200_less_than_high and ema200_greater_than_low))

    ##     
    def TDI_indicator(self, df):

        rsiPeriod = 21
        bandLength = 34
        lengthrsipl = 2
        lengthtradesl = 7 

        r = rsi(df['close'], window=rsiPeriod, fillna=True) #change n by window
        ma = r.rolling(window=bandLength).mean()
        offs = 1.6185 * r.rolling(window=bandLength).std()

        up = ma + offs                                                        
        dn = ma - offs                                                        
        mid = (up + dn) / 2              
        fastMA = r.rolling(window=lengthrsipl).mean()                      
        slowMA = r.rolling(window=lengthtradesl).mean()  

        df['mid'] = mid
        df['fast_sma'] = fastMA
        df['slow_sma'] = slowMA

        df['diff_sma'] = abs(df.fast_sma - df.slow_sma)

        return df
    
    def get_validation_tdi(self, candles_data):
    
        df = self.TDI_indicator(candles_data) 

        fast_sma_ob = df['fast_sma'].iloc[-(self.n_order_blocks+1)]
        low_sma_ob = df['slow_sma'].iloc[-(self.n_order_blocks+1)]
        mid_ob = df['mid'].iloc[-(self.n_order_blocks+1)]
        
        range_fast_sma = (fast_sma_ob <= mid_ob + self.TDI_RANGE) and (fast_sma_ob >= mid_ob - self.TDI_RANGE)
        range_slow_sma = (low_sma_ob  <= mid_ob  + self.TDI_RANGE) and (low_sma_ob  >= mid_ob  - self.TDI_RANGE)

        range_diff_smas = df['diff_sma'].iloc[-(self.n_order_blocks+1)] <= self.SMA_RANGE

        return range_fast_sma and range_slow_sma and range_diff_smas 
                  
    def get_candles_data_to_detect_OB(self):
        """ get the candles data needed to process the strategy """
    
        n_candles = self.get_candles_needed_for_running()
        candles = self.prices_repository.get_candles(
            self.base_currency, 
            self.quote_currency, 
            MINUTES_TO_BINANCE_INTERVAL[240], 
            n_candles)

        return candles
    
    def get_candles_needed_for_running(self):
        return self.MAX_NUMBER_OF_CANDLES

