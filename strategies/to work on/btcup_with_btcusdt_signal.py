# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from datetime import datetime
from time import sleep

from recommendations.strategies.candlestick import BinancePricesRepository, MINUTES_TO_BINANCE_INTERVAL
from recommendations.strategies.pair_strategies import *
from recommendations.strategies.now_better_than_before_strategy import *
from recommendations.strategies.volume_profile_strategy import *
from recommendations.strategies.bull_market_grid_strategy import *

class StrategyforBTCUPBTC_withBTCUSDTSignal(Holding):
    def __init__(self,
                 base_currency: str, 
                 quote_currency: str,
                 time_frame: int,
                 strategy: str,
                 prices_repository=BinancePricesRepository(),
                 **kwargs):
                 
        super().__init__(base_currency, quote_currency, time_frame, prices_repository)         
        self.Strategy = eval(strategy)
        self.kwargs = kwargs 
        
        
    def run(self, last_recommendation:float = 0):
        open_prices = self.get_candles_open_data()
        timestamp = open_prices.index[-1]
        price_variation = self.calculate_prices_variation(open_prices)
        recommendation, timestamp, _ = self.Strategy(
                                            base_currency = "BTC", 
                                            quote_currency = "USDT",
                                            time_frame = self.time_frame, 
                                            prices_repository = self.prices_repository,
                                            **self.kwargs
                                            ).run(last_recommendation)
        return recommendation, timestamp, price_variation
    
    def get_candles_open_data(self):
        """ get the candles data needed to process the strategy """

        candles_base = self._request_candles(self.base_currency)
        candles_quote = self._request_candles(self.quote_currency)
        
        return (candles_base["open"]/candles_quote["open"])

    def _request_candles(self, currency): 
        n_candles = self.get_candles_needed_for_running()
        binance_period = MINUTES_TO_BINANCE_INTERVAL[self.time_frame]
        
        candles = self.prices_repository.get_candles(currency, "USDT", binance_period, n_candles)
        counter = 0
        while candles["volume"][-1] == 0 and counter < 60:
            sleep(1)
            candles = self.prices_repository.get_candles(currency, "USDT", binance_period, n_candles)
            counter += 1
        
        return candles