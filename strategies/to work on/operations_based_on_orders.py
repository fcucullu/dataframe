
import builtins
from recommendations.strategies.pair_strategies import Strategy
from recommendations.strategies.candlestick import BinancePricesRepository

PENDING = 'pending'
BUY_EXECUTED = 'buy_executed'
SELL_EXECUTED = 'sell_executed'

class Order(Strategy):

    def __init__(self, base_currency: str, quote_currency: str, time_frame: int,\
        buy_limit, stop_loss, sell_limit, order_state, storage, prices_repository=BinancePricesRepository()):
        super().__init__(base_currency, quote_currency, time_frame, prices_repository)
        self.storage = storage.copy()
        self.storage.setdefault('buy_limit', float(buy_limit))
        self.storage.setdefault('stop_loss', float(stop_loss))
        self.storage.setdefault('sell_limit', float(sell_limit))
        self.storage.setdefault('order_state', order_state)
    
    def run(self, last_recommendation=None):
        candles_data = self.get_candles_data()
        open_prices = candles_data.open         
        proportion_to_hold=1
        timestamp = open_prices.index[-1]

        if self.storage['order_state'] == PENDING:
            proportion_to_hold = self.check_buying(open_prices)
        elif self.storage['order_state'] == BUY_EXECUTED:
            proportion_to_hold = self.check_selling(open_prices)
        else:
            proportion_to_hold = 0

        return proportion_to_hold, timestamp, self.calculate_prices_variation(open_prices), self.storage 

    def check_buying(self, open_prices):
        reco = 0 
        if open_prices[-1] < self.storage['buy_limit']:
            reco = 1
            self.storage['order_state'] = BUY_EXECUTED
        return reco

    def check_selling(self, open_prices):
        take_profit = open_prices[-1] > self.storage['sell_limit']
        stop_loss = open_prices[-1] < self.storage['stop_loss']
        reco = 1 
        if take_profit or stop_loss:
            reco = 0
            self.storage['order_state'] = SELL_EXECUTED

        return reco

    def run_over_a_df(self, candles):
        return super().run_over_a_df(candles)

