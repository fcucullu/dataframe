import pandas as pd

from recommendations.strategies.candlestick import BinancePricesRepository, MINUTES_TO_BINANCE_INTERVAL

class BuyAfterFalls:
    
    def __init__(
        self,
        base_currency: str, 
        quote_currency: str,
        first_take_profit=0.05, 
        second_take_profit=0.08,
        stop_loss=0.9, 
        fall=0.03, 
        span_between_payed_prices=0.04,
        candles_to_check_fall=6,
        time_frame=30,
        csv_path=None,
        PricesRepository=BinancePricesRepository):
        """
         - time_frame for candleticks in minutes
        """
        self.base_currency = base_currency 
        self.quote_currency = quote_currency        
        self.first_take_profit = first_take_profit
        self.second_take_profit = second_take_profit
        self.stop_loss = stop_loss
        self.fall = fall
        self.span_between_payed_prices = span_between_payed_prices
        self.candles_to_check_fall = candles_to_check_fall
        self.time_frame = time_frame
        
        self.csv_path = csv_path if csv_path is not None else f"{base_currency}{quote_currency}_timeframe_{time_frame}.csv"
        self.price_repository = PricesRepository()  
    
    def run(self, **kwargs):
        operations_data = self._read_csv_file()
        operations_data = self._update_candle_data(operations_data)
        operations_data = self._process_fall(operations_data)
        operations_data = self._process_buy(operations_data)
        operations_data = self._process_sell(operations_data)
        
        self._to_csv_file(operations_data)

        return operations_data.iloc[-1]    

    def _to_csv_file(self, operations_data):
        operations_data.to_csv(self.csv_path)


    def _read_csv_file(self):
        try:
            operations_data = pd.read_csv(self.csv_path, index_col=0)
        except:
            operations_data = self._initialize_csv_file()
        
        return operations_data
        
        
    def _initialize_csv_file(self):
        """ Creates the initial csv with data for processing strategy operations.
        """

        candles = self.price_repository.get_candles(
            self.base_currency, 
            self.quote_currency, 
            MINUTES_TO_BINANCE_INTERVAL[self.time_frame], 
            self.candles_to_check_fall)

        candles['fall'] = None
        candles['buy'] = False
        candles['sell'] = False
        candles['last_paid_price'] = None
        candles['n_positions'] = 0
        candles['capital'] = 1
        candles['paid_price_1'] = None
        candles['paid_price_2'] = None
        candles['weight_1'] = 0
        candles['weight_2'] = 0
        candles['operate'] = True

        return candles[:-1]

    def _update_candle_data(self, operations_data):
        new_candle_data = self.price_repository.get_candles(
            self.base_currency, 
            self.quote_currency, 
            MINUTES_TO_BINANCE_INTERVAL[self.time_frame], 
            1)
        
        # Default values, ie. no operations
        new_candle_data['fall'] = None
        new_candle_data['buy'] = False
        new_candle_data['sell'] = False        
        new_candle_data['last_paid_price'] = operations_data['last_paid_price'].iloc[-1]
        new_candle_data['capital'] = operations_data['capital'].iloc[-1]
        new_candle_data['paid_price_1'] = operations_data['paid_price_1'].iloc[-1]
        new_candle_data['paid_price_2'] = operations_data['paid_price_1'].iloc[-1]
        new_candle_data['weight_1'] = 0
        new_candle_data['weight_2'] = 0
        new_candle_data['n_positions'] = operations_data['n_positions'].iloc[-1]
        new_candle_data['operate'] = operations_data['operate'].iloc[-1]        

        operations_data = pd.concat([operations_data, new_candle_data])

        # update weights
        price_variation = operations_data.open[-1] / operations_data.open[-2]
        if operations_data['n_positions'].iloc[-1] >= 1:
            operations_data['weight_1'].iloc[-1] = operations_data['weight_1'].iloc[-2] * price_variation 
        if operations_data['n_positions'].iloc[-1] == 2:
            operations_data['weight_2'].iloc[-1] = operations_data['weight_2'].iloc[-2] * price_variation
        
        return operations_data

    def _process_fall(self, operations_data):
        variation = (operations_data.open[-self.candles_to_check_fall] / operations_data.open[-1]) - 1
        operations_data['fall'].iloc[-1] = variation > self.fall

        return operations_data

    
    def _process_buy(self, operations_data):
        #p_value, p_fall, q_value, q_fall, n_positions, last_price_payed, operate):
        buy = False
        last_paid_price = operations_data.last_paid_price.iloc[-2]
        n_positions = operations_data.n_positions.iloc[-2]
        current_price = operations_data.open.iloc[-1]
        
        # the current price has to be "far" from last paid price to operate       
        if last_paid_price is not None:
            difference_wrt_payed_price = (current_price / last_paid_price) - 1 
            if difference_wrt_payed_price >= -self.span_between_payed_prices:
                return operations_data
        
        there_was_a_fall_two_candles_ago = operations_data.fall.iloc[-3]
        current_price_is_fall = operations_data.fall.iloc[-1]
        price_two_candles_ago = operations_data.open.iloc[-3]
        
        if there_was_a_fall_two_candles_ago:
            difference_btwn_current_price_and_price_after_fall = (
                (current_price / price_two_candles_ago) - 1
            )
            print(difference_btwn_current_price_and_price_after_fall, current_price_is_fall)
            if difference_btwn_current_price_and_price_after_fall > -0.02 and not current_price_is_fall:
                operations_data['buy'].iloc[-1] = True
                operations_data['last_paid_price'].iloc[-1] = operations_data.open.iloc[-1]
                n_positions = operations_data['n_positions'].iloc[-2]
                operations_data['n_positions'].iloc[-1] = n_positions + 1
                operations_data[f'paid_price_{int(n_positions + 1)}'].iloc[-1] = operations_data.open.iloc[-1]
                if int(n_positions + 1) == 1:
                    operations_data['capital'].iloc[-1] = operations_data[f'capital'].iloc[-1] / 2 
                    operations_data[f'weight_{int(n_positions + 1)}'].iloc[-1] = operations_data[f'capital'].iloc[-1]
                else:
                    operations_data[f'weight_{int(n_positions + 1)}'].iloc[-1] = operations_data[f'capital'].iloc[-1]
                    operations_data['capital'].iloc[-1] = 0

 
        return operations_data

    def _process_sell(self, operations_data):
        open_price = operations_data.open[-1] 
        last_high = operations_data.high[-2] # last row is current candle
        last_paid_price = operations_data.last_paid_price[-2]
        n_positions = operations_data.n_positions[-2]
        
        if n_positions == 0:
            return operations_data

        if n_positions == 1:
            sell = last_high >= last_paid_price * (1 + self.first_take_profit)
            operate = True
        else:
            sell_and_win = last_high >= last_paid_price * (1 + self.second_take_profit)
            sell_and_stop_operate = open_price <= last_paid_price * self.stop_loss
            sell = sell_and_win or sell_and_stop_operate
            operate = not sell_and_stop_operate

        operations_data['sell'].iloc[-1] = sell
        operations_data['operate'].iloc[-1] = operate
        operations_data['n_positions'].iloc[-1] = 0

        operations_data['capital'].iloc[-1] = round(
            operations_data['capital'].iloc[-1] + operations_data['weight_1'].iloc[-1] + operations_data['weight_2'].iloc[-1],
            2
        )
        operations_data['weight_1'].iloc[-1] = 0
        operations_data['weight_2'].iloc[-1] = 0


        return operations_data

