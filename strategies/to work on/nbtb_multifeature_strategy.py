import numpy as np
#from talib import ATR
from recommendations.strategies.now_better_than_before_strategy import BullMarketNowBetterThanBeforeDynamicParametersWithMovingAverage
from recommendations.strategies.candlestick import BinancePricesRepository

class NBTBMultiFeatureStd(BullMarketNowBetterThanBeforeDynamicParametersWithMovingAverage):
        
    def __init__(self, 
        base_currency: str, 
        quote_currency: str,
        time_frame: int, 
        sl_width: int,
        tp: float,
        storage=None, 
        moving_average: int = 1,
        bull_market_bull_factor: float=1.0,
        bull_market_bear_factor: float=1.0,
        bull_market_threshold_factor: float=1.015,
        bear_market_bull_factor: float=1.02,
        bear_market_bear_factor: float = 1.02,
        bear_market_threshold_factor: float = 1.05,
        n_for_tendency: int = 12,          
        active_operation_threshold: float = 0,
        prices_repository=BinancePricesRepository()):

        super().__init__(
            base_currency=base_currency, 
            quote_currency=quote_currency,
            time_frame=time_frame,
            storage=storage,
            moving_average=moving_average,
            bull_market_bull_factor=bull_market_bull_factor,
            bull_market_bear_factor=bull_market_bear_factor,
            bull_market_threshold_factor=bull_market_threshold_factor,
            bear_market_bull_factor=bear_market_bull_factor,
            bear_market_bear_factor=bear_market_bear_factor,
            bear_market_threshold_factor=bear_market_threshold_factor,
            n_for_tendency=n_for_tendency,          
            active_operation_threshold=active_operation_threshold,
            prices_repository=prices_repository,            
        )

        self.sl_width = sl_width
        self.tp = tp
                    
    def set_metric_for_sl(self, candles):
        return candles.open.rolling(30).std().fillna(method='bfill')
    
    def not_basic_signal(self, candles, i):
        return candles.basic_signal[i] == 0
    
    def is_long_basic_signal(self, candles, i):
        return candles.basic_signal[i] == 1
    
    def is_short_basic_signal(self, candles, i):
        return candles.basic_signal[i] == -1
    
    def is_new_long_basic_position(self, candles, i):
        if self.is_long_basic_signal(candles, i):
            return (candles.basic_signal[i] != candles.basic_signal[i-1]) 
        else:
            return False
        
    def is_reentry_long_position(self, candles, i):
        if self.is_long_basic_signal(candles, i):
            return (candles.stop_reentry[i] == 0) and (candles.stop_reentry[i-1] == 1)
        else:
            return False
        
    def check_tp_sl(self, candles, i):
        if candles.signal[i-1] == 1:
            candles.sl[i], candles.tp[i] = candles.sl[i-1], candles.tp[i-1]
        if (candles.open[i] > candles.sl[i]) and (candles.open[i] <= candles.tp[i]) and (candles.stop_reentry[i] != 1):
            candles.signal[i] = 1
        else:
            candles.signal[i] = 0
            if candles.stop_reentry[i-1] == 0:
                candles.stop_reentry[i:min(i+3,len(candles))] = 1
        return candles
                            
    def set_sl_tp_values(self, candles, i):
        candles.sl[i] = candles.open[i] - candles.sl_metric[i] * self.sl_width
        candles.tp[i] = candles.open[i] * (1+self.tp)
        candles.signal[i] = 1          
        return candles
        
    def check_threshold_and_takeprofit(self, candles):
        candles['signal'] = np.nan
        candles['sl'] = np.nan
        candles['tp'] = np.nan
        candles['stop_reentry'] = 0
        candles['sl_metric'] = self.set_metric_for_sl(candles)
        
        for i in range(len(candles)):
            if self.not_basic_signal(candles, i):
                candles.signal[i] = 0

            elif self.is_long_basic_signal(candles, i):
                beginning_of_trade = (self.is_new_long_basic_position(candles, i) or self.is_reentry_long_position(candles, i))
                if beginning_of_trade:
                    candles = self.set_sl_tp_values(candles, i)
                else:
                    candles = self.check_tp_sl(candles, i)
            
            elif self.is_short_basic_signal(candles, i):
                raise ValueError('Not prepared for short trades yet')
            
            else:
                raise ValueError('Signal column must be 1, 0 or -1')
        return candles
        
    

""" 
class NBTBMultiFeatureAtr(NBTBMultiFeatureStd):

    def set_metric_for_sl(self, candles):
        return ATR(candles.high, candles.low, candles.open, timeperiod=14).fillna(method='bfill')
"""

