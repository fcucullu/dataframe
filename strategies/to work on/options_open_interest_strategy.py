#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  1 10:17:21 2021

@author: farduh
"""

import pandas as pd
import numpy as np
from datetime import datetime
from datetime import timedelta
from ta.momentum import rsi
import requests
import time

from recommendations.strategies.candlestick import BinancePricesRepository
from recommendations.strategies.pair_strategies import Holding


class OptionsOpenInterestStrategy(Holding):
    def __init__(self, 
        base_currency: str, 
        quote_currency: str,
        time_frame: int, 
        prices_repository=BinancePricesRepository()):
        super().__init__(base_currency, quote_currency, time_frame, prices_repository)         
        self.base_currency = base_currency
        self.quote_currency = quote_currency
        
        
    def run(self, last_recommendation:float = 0):
        candles = self.get_candles_data()
        historical_OI = self.get_options_historical_OI_data()
        liquidation_OI = self.get_options_liquidation_OI_data()
        timestamp = candles.index[-1]
        price_variation = self.calculate_prices_variation(candles['open'])
        recommendation = self.calculate_recommendation(historical_OI,liquidation_OI)
        return recommendation, timestamp, price_variation
    
    def get_options_historical_OI_data(self):
        
        while True:
            apibybt = "c58bf25a12fe42c9804af42a665f4600"
            url = "http://open-api.bybt.com/"
            payload = {}
            headers = {
                'bybtSecret': apibybt,
                }
            response = requests.request("GET", f'{url}/api/pro/v1/option/openInterest/history/chart?symbol={self.base_currency}&interval=0', headers=headers, data = payload)
            df =self. from_json_to_dataframe(response)
            current_hour = datetime.now().hour
            current_day = datetime.now().day
            dataframe_lastday = df.index[-1].day
            dataframe_lasthour = df.index[-1].hour        
            if (current_hour == dataframe_lasthour) &\
                (current_day != dataframe_lastday) :
                print(current_day , dataframe_lastday)
                time.sleep(10)
                continue
            else:
                break
        return df
    
    def get_options_liquidation_OI_data(self):
        response = requests.get(f"https://fapi.bybt.com/api/option/chart?type=Delivery&ex=ALL&symbol={self.base_currency}&subtype=ALL")
        data = pd.DataFrame(response.json()["data"]["data"])
        data["keyList"] = data["keyList"].apply(lambda x : datetime.strptime(str(x),"%y%m%d"))
        data = data.set_index("keyList")
        return data
    
    def get_predicted_OI(self,historical_OI,liquidation_OI):
        variation_OI = self.next_day_linear_correction(historical_OI)
        next_day_liquidation_OI = self.next_day_liquidation(liquidation_OI, historical_OI)
        next_day_OI = historical_OI["Total"].iloc[-1]*(1+variation_OI)-next_day_liquidation_OI
        return next_day_OI
          
    def next_day_linear_correction(self,historical_OI):
        one_day_timestamp = datetime(2021,1,2).timestamp()-datetime(2021,1,1).timestamp()
        X=np.array([x.timestamp() for x in historical_OI[historical_OI["Total"].pct_change()>0].index[-7:]])
        y=np.array(np.log((1+historical_OI["Total"].pct_change()[historical_OI["Total"].pct_change()>0]).cumprod())[-7:].values)
        pendiente = np.polyfit(X,y,1)[0]
        OI_variation = pendiente*one_day_timestamp
        return OI_variation
    
    def next_day_liquidation(self,liquidation_OI,historical_OI):
        if not min(liquidation_OI.index) == max(historical_OI.index)+timedelta(days=1)-timedelta(hours=13):
            return 0
        next_day_liquidation_series = liquidation_OI.loc[min(liquidation_OI.index)]
        total_OI = next_day_liquidation_series["putOIList"] + next_day_liquidation_series["callOIList"]
        return total_OI
        
    def from_json_to_dataframe(self,response):
        df = pd.DataFrame()
        for key in response.json()['data'].keys():
            if key == 'dataMap':
                for key in response.json()['data']['dataMap'].keys():
                    df[key] = response.json()['data']['dataMap'][key]
            else:
                df[key] = response.json()['data'][key]
        df.dateList = df.dateList.apply(lambda x : datetime.fromtimestamp(x/1000.))
        df = df.set_index('dateList')
        df["price"] = df["priceList"]
        df = df.drop(columns="priceList")
        df["Total"]=df.drop(columns='price').sum(axis=1)
        df[df.drop(columns='price').columns] = df.drop(columns='price').multiply(1/df['price'],axis=0)
        return df
    
    def calculate_recommendation(self,historical_OI,linear_OI):
        completed_dataframe = historical_OI.copy()
        future_date = historical_OI.index[-1]+timedelta(days=1)
        predicted_OI = self.get_predicted_OI(historical_OI,linear_OI)
        completed_dataframe.loc[future_date,"Total"] = predicted_OI
        completed_dataframe = completed_dataframe.ffill()
        rsi_prices = rsi(completed_dataframe["price"],window=7)
        rsi_open_interest = rsi(completed_dataframe["Total"],window=7)
        bool_desition = rsi_prices>rsi_open_interest
        if bool_desition[-1]:
            recommendation = 1
        else:
            recommendation = 0
        return recommendation