#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 11 15:24:20 2021

@author: farduh
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 21 10:38:38 2021

@author: farduh
"""
from recommendations.strategies.coin_alyze import CoinAlyze
import pandas as pd
import numpy as np
from datetime import datetime
from ta.momentum import rsi
import time
from market_profile import MarketProfile
from recommendations.strategies.pair_strategies import Holding
from recommendations.strategies.candlestick import BinancePricesRepository

class FrOiStrategy(Holding):
    
    def __init__(self,         
                base_currency: str, \
                quote_currency: str,\
                time_frame: int, \
                window = 24,window_funding_rate=11,\
                gap=10,short=3,long=27,\
                prices_repository=BinancePricesRepository()):
        
        super().__init__(base_currency, quote_currency, time_frame, prices_repository)         
        self.coin = base_currency
        self.window = int(window)
        self.window_funding_rate = int(window_funding_rate)
        self.gap = round(gap)
        self.short = int(short)
        self.long = int(long)
        
    def run(self, last_recommendation:float = 0):
        
        candles = self.get_candles_data()
        timestamp = candles.index[-1]
        price_variation = self.calculate_prices_variation(candles['open'])
        
        df = self.get_data()   
        df_1 = pd.DataFrame(columns=["Open","Low","High","Close","Volume"])
        df_1[["Open","Low","High","Close","Volume"]] = df[["open","low","high","close","volume"]]
        mp = MarketProfile(df_1,   value_area_pct= 0.6     )
        df["poc"] = np.append(np.full(100, np.nan),np.array([mp[i-100:i].poc_price for i in range(100,len(df))]))
        
        df["rsi_oi"] = rsi(df["oi_twap"].pct_change().rolling(21).std(),window=self.window)
        df["rsi"] = rsi(df["price_twap"].pct_change().rolling(21).std(),window=self.window)
        df["rsi_fr"] = rsi(df["fr_twap"],window=self.window_funding_rate)
        df["actions"] = np.nan
        df.loc[(df["rsi"]>df["rsi_oi"])&(df["rsi_fr"]<(50-self.gap))&(df["rsi"].pct_change().rolling(3).mean()>0),"actions"] = (1)
        df.loc[(df["rsi"]>df["rsi_oi"])&(df["rsi_fr"]>(50+self.gap))&(df["rsi"].pct_change().rolling(3).mean()>0),"actions"] = (-1)
        
        status = df["actions"].ffill()
        df["ma_short"] = (df["price_twap"]).rolling(self.short).mean()
        ma_long = df["poc"] 
        ma_short = df["ma_short"]
        df["actions"] = np.where((status==1) & (ma_long>=ma_short) & (ma_long<ma_short).shift(1),0, df["actions"])
        df["actions"] = np.where((status==1) & (ma_long<ma_short) & (ma_long>ma_short).shift(1),1, df["actions"])
        df["actions"] = np.where((status==-1) & (ma_long>=ma_short) & (ma_long<ma_short).shift(1),-1, df["actions"])
        df["actions"] = np.where((status==-1) & (ma_long<ma_short) & (ma_long>ma_short).shift(1),0, df["actions"])
    
        df["actions"] = df["actions"].ffill()
        
        recommendation = last_recommendation
        if df["actions"].iloc[-1]==1 and df["actions"].iloc[-2]!=1:
            recommendation = 1
        elif df["actions"].iloc[-1]!=1 and df["actions"].iloc[-2]==1:
            recommendation = 0
        return recommendation, timestamp, price_variation

    def get_data(self):
        coin_alyze = CoinAlyze(60,self.coin)
        is_not_last_value = True
        last_hour = datetime.now().hour
        while is_not_last_value:
            df = coin_alyze.get_candlesticks()
            if df.index[-1].hour ==last_hour:
                is_not_last_value = False
            time.sleep(10)
        df = df.join(coin_alyze.get_open_interests())
        df = df.join(coin_alyze.get_funding_rates())
        df["oi_twap"] = (df["oi_high"]+df["oi_low"]+df["oi_close"])/3
        df["price_twap"] = (df["high"]+df["low"]+df["close"])/3
        df["fr_twap"] = (df["fr_high"]+df["fr_low"]+df["fr_close"])/3
        df = df.shift(1)
        return df 

