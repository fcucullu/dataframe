from datetime import datetime, timedelta
import pandas as pd
import numpy as np
from decimal import Decimal as D
from recommendations.indicators.volume_profile_areas import VPAreas
from recommendations.indicators.market_classifier import MarketClassifier
from recommendations.strategies.pair_strategies import Strategy
from recommendations.strategies.candlestick import CandlestickRepository, BinancePricesRepository, MINUTES_TO_BINANCE_INTERVAL

class TendencyplusVPStrategy(Strategy):
    def __init__(self, 
                 base_currency: str,
                 quote_currency: str,
                 time_frame: int,
                 storage: dict = dict(),
                 market_classification_method='mean',
                 prices_repository=BinancePricesRepository()):
        self.base_currency = base_currency
        self.quote_currency = quote_currency
        self.time_frame = int(time_frame)
        self.prices_repository = prices_repository
        self.storage = storage
        self.market_classification_method = market_classification_method
        
    def run(self, last_recommendation=0):
        
        timestamp,last_open_price,last_open_pct = self.get_open_prices_info()
        
        if not self.storage:
            candles_1d, candles_1m = self.get_1d_1m_candles_from_influx()
            previous_tendency = 0
            self.init_storage_and_set_market_classification(candles_1d, candles_1m)
        else:
            candles_1d, candles_1m = self.get_1d_1m_candles()
            previous_tendency = self.storage['tendency']
            self.update_storage(candles_1m)
            self.set_market_classification(candles_1d.iloc[-8:])
        
        recommendation = self.get_recommendation(last_recommendation,previous_tendency,timestamp,last_open_price)
        
        return recommendation, timestamp, last_open_pct ,self.storage
    
    def get_open_prices_info(self):
        candles_tf = self.prices_repository.get_candles(
            self.base_currency, 
            self.quote_currency, 
            MINUTES_TO_BINANCE_INTERVAL[self.time_frame], 
            2)
        last_open_prices = candles_tf.open
        last_open_price = last_open_prices.iloc[-1]
        last_open_pct = D(last_open_prices.iloc[-1]/last_open_prices.iloc[-2])
        timestamp = last_open_prices.index[-1]
        
        return timestamp,last_open_price,last_open_pct
    
    def get_1d_1m_candles_from_influx(self,start_date=datetime(2020,1,1),end_date=datetime.utcnow()):
        pair = self.base_currency+'/'+self.quote_currency
        repo = CandlestickRepository.default_repository()
        candles_1d = repo.get_candlestick(pair, 'binance', 24*60, start_date, end_date)
        candles_1m = repo.get_candlestick(pair, 'binance', 1, start_date, end_date)
        candles_1m = candles_1m.iloc[:-1]
        candles_1d = candles_1d.iloc[:-1]
        #candles_1d.index = [x.astimezone(None) for x in candles_1d.index]
        #candles_1m.index = [x.astimezone(None) for x in candles_1m.index]
        return candles_1d, candles_1m
    
    def init_storage_and_set_market_classification(self,candles_1d, candles_1m):
        vpareas = VPAreas(candles_1m,minutes_in_candles=60)
        tick_size = vpareas.get_tick_size()
        profile = vpareas.get_last_profile()    
        profile_dictionaty = profile.to_json()
        mktcla = MarketClassifier(method=self.market_classification_method)
        mktcla.fit(candles_1d)
        storage = {'profile':profile_dictionaty,
                   'tick_size':tick_size,
                   'mkt_classifier_requirement':mktcla.requirement,
                   'thresholds': {'high2':np.nan,
                                  'high1':np.nan,
                                  'low2':np.nan,
                                  'low1':np.nan}}
        self.storage = storage
        self.set_market_classification(candles_1d.iloc[-8:])
        self.update_threshold(candles_1d['open'].iloc[-1],0,0)    
        
    def get_1d_1m_candles(self):
        candles_1d = self.prices_repository.get_candles(
            self.base_currency, 
            self.quote_currency,
            '1d', 
            8)
        candles_1m = self.prices_repository.get_candles(
            self.base_currency, 
            self.quote_currency, 
            '1m', 
            self.time_frame+1)
        candles_1m = candles_1m.iloc[:-1]
        candles_1d = candles_1d.iloc[:-1]
        return candles_1d, candles_1m
    
    def update_storage(self,candles_1m):
        self.update_profile(candles_1m)
        
    def update_profile(self,candles_1m):
        online_profile = pd.read_json(self.storage['profile'],convert_axes=False)
        online_profile.index = online_profile.index.astype(float)
        online_profile = online_profile.sort_index()
        tick_size = self.storage['tick_size']
        vpareas = VPAreas(candles_1m,minutes_in_candles=60,tick_size=tick_size)
        last_hour_profile = vpareas.get_last_profile()
        online_profile = online_profile.add(last_hour_profile,fill_value=0)
        self.storage['profile'] = online_profile.to_json()
    
    def set_market_classification(self,candles_1d):
        mktcla = MarketClassifier(method=self.market_classification_method)
        mktcla.set_requirement(self.storage['mkt_classifier_requirement'])
        clusters = mktcla.predict(candles_1d)
        self.market_classification = clusters.iloc[-1]
        self.storage.update({'tendency' : clusters.iloc[-1]})
    
    def get_recommendation(self,last_recommendation,previous_tendency,timestamp,last_open_price):
        
        if timestamp.hour == 0:
            self.update_threshold(last_open_price,last_recommendation,previous_tendency)
        if self.market_classification==1:
            threshold = self.storage['thresholds']['low2']
        else:
            threshold = self.storage['thresholds']['high2']
        if last_open_price > threshold and self.market_classification != 0:
            recommendation = 1
        else:
            recommendation = 0
        
        return recommendation
    
    def update_threshold(self,last_open_price,last_recommendation,previous_tendency):
        
        filtered_profile = self.get_filtered_profile(last_open_price)
        filtered_profile['mean_price'] = pd.Series(filtered_profile.index).ewm(2).mean().values
        
        thresholds = {}
        for threshold_type in ['high1','high2','low1','low2']:
            threshold = self.get_threshold_by_type(threshold_type,filtered_profile,last_open_price)
            thresholds.update({threshold_type:threshold})
        self.storage['thresholds'] = thresholds
        
        
    def get_threshold_by_type(self,threshold_type,filtered_profile,last_open_price):
        
        if 'high' in threshold_type:
            filtered_profile = filtered_profile[(filtered_profile.index<last_open_price)].sort_index(ascending=False)
        else:
            filtered_profile = filtered_profile[(filtered_profile.index>last_open_price)]
        
        if '2' in threshold_type :
            if len(filtered_profile.loc[filtered_profile['type']=='max','mean_price'])>2:
                threshold = filtered_profile.loc[filtered_profile['type']=='max','mean_price'].iloc[2]
            elif len(filtered_profile.loc[filtered_profile['type']=='max','mean_price'])>1 :
                diff_mean = filtered_profile['mean_price'].diff().mean()
                if 'high' in threshold_type:
                    threshold = filtered_profile['mean_price'].iloc[-1]+2*diff_mean
                else:
                    threshold = filtered_profile['mean_price'].iloc[-1]-2*diff_mean
            else:
                if 'high' in threshold_type:
                    variation = 1+4*self.storage['tick_size']
                else:
                    variation = 1-4*self.storage['tick_size']
                threshold = last_open_price*variation
        elif '1' in threshold_type :
            if len(filtered_profile.loc[filtered_profile['type']=='max','mean_price'])>1:
                threshold = filtered_profile.loc[filtered_profile['type']=='max','mean_price'].iloc[1]
            else:
                if 'high' in threshold_type:
                    variation = 1+2*self.storage['tick_size']
                else:
                    variation = 1-2*self.storage['tick_size']
                threshold = last_open_price*variation
        return threshold
    
    def get_filtered_profile(self,last_open_price):
        online_profile = pd.read_json(self.storage['profile'],convert_axes=False)
        online_profile.index = online_profile.index.astype(float)
        online_profile = online_profile.sort_index()
        vpareas = VPAreas(pd.DataFrame(columns=['open', 'high', 'low', 'close']),tick_size=self.storage['tick_size'])
        filtered_profile = vpareas.keep_min_and_max(online_profile)
        return filtered_profile
    
    def run_over_a_df(self,df):
        recommendation = 0
        candles_1d, candles_1m = self.get_1d_1m_candles_from_influx(start_date=datetime(df.index[0].year-1,1,1),end_date=df.index[-1])
        self.init_storage_and_set_market_classification(candles_1d.loc[:df.index[0]].iloc[:-1], candles_1m.loc[:df.index[0]].iloc[:-1])
        df['signal'] = np.nan
        df['tendency'] = np.nan
        df['threshold'] = np.nan
        df['filtered_profile'] = np.nan
        list_filtered_profile = []
        list_thresholds = []
        for idx,row in df.iterrows():
            previous_tendency = self.storage['tendency']
            self.update_storage(candles_1m.loc[:idx].iloc[-1-self.time_frame:-1])
            self.set_market_classification(candles_1d.loc[:idx].iloc[-8:-1])
            recommendation = self.get_recommendation(recommendation,previous_tendency,idx,row['open'])
            df.loc[idx,'signal'] = recommendation
            df.loc[idx,'tendency'] = self.storage['tendency']
            online_profile = pd.read_json(self.storage['profile'],convert_axes=False)
            online_profile.index = online_profile.index.astype(float)
            online_profile = online_profile.sort_index()
            vpareas = VPAreas(pd.DataFrame(columns=['open', 'high', 'low', 'close']),tick_size=self.storage['tick_size'])
            filtered_profile = vpareas.keep_min_and_max(online_profile)
            list_filtered_profile.append(filtered_profile)
            list_thresholds.append(self.storage['thresholds'])
        df['filtered_profile'] = list_filtered_profile
        df['thresholds'] = list_thresholds
        return df
