import pandas as pd
import numpy as np
from datetime import datetime
from recommendations.strategies.dominance import Dominance
from recommendations.strategies.candlestick import BinancePricesRepository, MINUTES_TO_BINANCE_INTERVAL
from recommendations.strategies.pair_strategies import *
from recommendations.strategies.now_better_than_before_strategy import *
from recommendations.strategies.volume_profile_strategy import *
from recommendations.strategies.bull_market_grid_strategy import *

class DominanceChecker(Holding):
    def __init__(self,
                 base_currency: str, 
                 quote_currency: str,
                 time_frame: int,
                 strategy: str,
                 stable_coins=["USDT"],
                 alt_coins=["ADA","DOT","DOGE","BNB","XRP"],
                 prices_repository=BinancePricesRepository(),
                 **kwargs):
                 
        super().__init__(base_currency, quote_currency, time_frame, prices_repository)         
        Strategy = eval(strategy)
        self.strategy = Strategy(base_currency = self.base_currency, 
                    quote_currency = self.quote_currency,
                    time_frame = self.time_frame, 
                    prices_repository = self.prices_repository,
                    **kwargs
                    )
        self.base_currency = base_currency
        self.quote_currency = quote_currency            
        self.dominance = Dominance(stable_coins=stable_coins,alt_coins=alt_coins,periods=300)
        
        
        
    def run(self, last_recommendation:float = 0):
        recommendation, timestamp, price_variation = self.strategy.run(last_recommendation)
        if last_recommendation == 0:
            dominance_signal = self.dominance.get_btc_dominance_increasing_signal()
            if dominance_signal :
                recommendation = 0
            
        return recommendation, timestamp, price_variation
    
    def run_over_a_df(self,candles):
        candles = self.strategy.run_over_a_df(candles)
        datenow = datetime.utcnow()
        datenow = datenow.astimezone(candles.index[0].tz)
        total_periods = (datenow-candles.index[0]).total_seconds()/(60*5)
        total_periods = int(total_periods)+300
        self.dominance.periods = total_periods        
        indicators = self.dominance.get_btc_dominance_increasing_indicators()
        prev_signal = 0
        clean_trade = False
        for i,row in candles.iterrows():
            if row['signal'] != prev_signal and row['signal'] == 1:
                dominance_signal = indicators.loc[i,'signal']
                if dominance_signal:
                    candles.loc[i,'signal'] = 0
                    clean_trade = True
            if clean_trade and row['signal'] == 1:
                candles.loc[i,'signal'] = 0
            else: 
                clean_trade = False
            prev_signal = row['signal']
        return candles