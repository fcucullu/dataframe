import pandas as pd
import numpy as np
from datetime import datetime, timezone
from recommendations.strategies.candlestick import CandlestickRepository
import recommendations.utils.simulations as simulations

from recommendations.strategies.candlestick import BinancePricesRepository, MINUTES_TO_BINANCE_INTERVAL
from recommendations.strategies.pair_strategies import Holding
from recommendations.indicators.get_trend_by_sq import GetIndicators
from recommendations.indicators.volume_profile import VolumeProfileIndicator

class SqueezeStrategy(Holding):
    
    LONG = 1
    CLOSE_LONG = 0

    MAX_NUMBER_OF_CANDLES = 24*7
    COLUMN = 'open'
    N_ADX_ROLLING = 14
    
    ## Init
    def __init__(self, 
        base_currency: str, 
        quote_currency: str,
        time_frame: int, 
        adx_condition: bool, 
        state_condition: bool,
        prices_repository=BinancePricesRepository()):

        super().__init__(base_currency, quote_currency, time_frame, prices_repository)      
        self.adx_condition = adx_condition
        self.state_condition = state_condition 

    
    ## Run
    def run(self, last_recommendation: float =0, **kwargs):
        """ 
        return strategy status. Value 1 means hold, 0 means do not hold. 
        """

        candles_data = self.get_candles_data()
        candles_data = self.increase_values_to_significant_magnitudes(candles_data)
        
        timestamp = candles_data.index[-1]

        price_variation = self.calculate_prices_variation(candles_data['open'])        
    
        recommendation = self.calculate_recommendation(candles_data)
        if recommendation is None:
            recommendation = last_recommendation

        return recommendation, timestamp, price_variation

    ## Calculate Reco  
    def calculate_recommendation(self, original_candles):

        candles = self.run_over_a_df(original_candles)
        
        current_state = candles['signal'].iloc[-1]
        previous_state = candles['signal'].iloc[-2]
        change_of_state =  current_state != previous_state
        
        recommendation = None
        
        if change_of_state and candles['signal'].iloc[-1] == self.LONG:
            # Solo se manda long (1) cuando se genera la señal sino no se manda nada 
            recommendation = 1
        elif candles['signal'].iloc[-1] == self.CLOSE_LONG:
            # Si la señal actual es close, siempre mando la señal. 
            recommendation = 0

        return recommendation

    #get all data
    def process_candles(self, original_candles):
    
        candles = original_candles.copy()
        
        start_time, end_time = candles.index[0], candles.index[-1]

        Indicators = GetIndicators( self.base_currency, 
                                    self.quote_currency, 
                                    self.time_frame, 
                                    start_time, 
                                    end_time) 
        
        candles = Indicators.adx(candles, self.N_ADX_ROLLING)
        
        #get volume profile
        volume_profiles = VolumeProfileIndicator(self.base_currency, 
                                                 self.quote_currency, 
                                                 self.time_frame, 
                                                 2, 
                                                 .7, 
                                                 .05,
                                                 candles.index[0],
                                                 candles.index[-1])
        

        candles_vp = self.get_more_candles_data(1/12, candles.index[0], candles.index[-1])

        df_vp = volume_profiles.get_general_volume_profile( 2*12*24, 
                                                            candles_vp.open, 
                                                            candles_vp.close, 
                                                            candles_vp.high, 
                                                            candles_vp.low, 
                                                            candles_vp.volume, 
                                                            .7,
                                                            .05)
        
        candles.index = pd.DatetimeIndex([i.replace(tzinfo=None) for i in candles.index]) 

        #get df complete
        for cols in df_vp.columns:
            candles.loc[df_vp.index, cols] = df_vp[cols]
        candles = candles.fillna(method='ffill')

        candles.loc[candles.index[0]:, 'poc'] = df_vp.loc[candles.index[0]:, 'poc']

        candles['ema_10'] = candles['open'].ewm(span = 10, adjust = False).mean()
        candles['ema_55'] = candles['open'].ewm(span = 55, adjust = False).mean()

        candles = self.get_states(candles)
        return candles
    
    def get_states(self, candles):
        
        start_time, end_time = candles.index[0], candles.index[-1]
    
        Indicators = GetIndicators(self.base_currency, 
                             self.quote_currency, 
                             self.time_frame, 
                             start_time, 
                             end_time) 
    
        df = Indicators.squezee(candles, self.COLUMN)
                
        df['Estado'] = np.where((df['Monitor']>0) & (df['Monitor']<df['Monitor'].shift()), 
                                'verde_oscuro', 
                                0)
        df['Estado'] = np.where((df['Monitor']>0) & (df['Monitor']>df['Monitor'].shift()), 
                                'verde_claro', 
                                df['Estado'])
        df['Estado'] = np.where((df['Monitor']<0) & (df['Monitor']>df['Monitor'].shift()), 
                                'rojo_oscuro', 
                                df['Estado'])
        df['Estado'] = np.where((df['Monitor']<0) & (df['Monitor']<df['Monitor'].shift()), 
                                'rojo_claro', 
                                df['Estado'])

        return df
    
    def check_conditions_to_open_a_position(self, df):
    
        criterion_monitor = df['Monitor'] > df['Monitor'].shift()
        criterion_adx = df['adx'] > df['adx'].shift()
        criterion_ema = df['ema_10'] > df['ema_55']
        criterion_poc = df['open'] > df['poc']

        criteria_buy = criterion_monitor & criterion_adx & criterion_ema & criterion_poc
                
        if self.adx_condition:
            criteria_buy = criteria_buy & (df['adx'] > 23)
               
        if self.state_condition:
            df = self.get_state_conditions(df)
            criteria_buy = criteria_buy & (df['General_state'] == 1)
            
        df['signal'] = np.where(criteria_buy, 1, df['signal'])

        return df 
    
    def check_conditions_to_close_a_position(self, df):

        criterion_monitor = df['Monitor'] < df['Monitor'].shift()
        criterion_adx = df['adx'] < df['adx'].shift()
        criterion_poc = df['open'] < df['poc']

        criteria_sell = criterion_monitor & criterion_adx & criterion_poc 
        df['signal'] = np.where(criteria_sell, 0, df['signal']) 
        
        return df 
        
    
    def get_state_conditions(self, df_1h):
        
        candles_4h = self.get_more_candles_data(time_frame_in_hours = 4, 
                                                start_date = df_1h.index[0],     
                                                end_date = df_1h.index[-1])
    
        candles_8h = self.get_more_candles_data(time_frame_in_hours = 8, 
                                                start_date = df_1h.index[0],     
                                                end_date = df_1h.index[-1])

        candles_4h.index = pd.DatetimeIndex([i.replace(tzinfo=None) for i in candles_4h.index]) 
        candles_8h.index = pd.DatetimeIndex([i.replace(tzinfo=None) for i in candles_8h.index]) 

        candles_4h = self.increase_values_to_significant_magnitudes(candles_4h)
        candles_8h = self.increase_values_to_significant_magnitudes(candles_8h)
        
        df_state_4h = self.get_states(candles_4h)
        df_state_8h = self.get_states(candles_8h)
        
        df_state = df_1h.copy()        
        df_state = df_state.rename(columns = {'Estado': 'State_1h'})

        df_state['State_4h'] = df_state_4h['Estado']
        df_state['State_8h'] = df_state_8h['Estado']
        
        #df_state.loc[df_state_4h.index,'State_4h'] = df_state_4h.loc[df_state_4h.index,'Estado']
        #df_state.loc[df_state_8h.index,'State_8h'] = df_state_8h.loc[df_state_8h.index,'Estado']
        
        df_state['State_4h'] = df_state.State_4h.mask(df_state.State_4h=='nan', None).ffill()
        df_state['State_8h'] = df_state.State_8h.mask(df_state.State_8h=='nan', None).ffill()
        
        c1 = (df_state.State_4h == 'rojo_oscuro') & (df_state.State_8h == 'rojo_oscuro')
        c2 = (df_state.State_4h == 'rojo_oscuro') & (df_state.State_8h == 'verde_claro')
        c3 = (df_state.State_4h == 'verde_claro') & (df_state.State_8h == 'verde_claro')
        c4 = (df_state.State_4h == 'verde_claro') & (df_state.State_8h == 'rojo_oscuro')
        c5 = (df_state.State_4h == 'verde_claro') & (df_state.State_8h == 'verde_oscuro')
        c6 = (df_state.State_4h == 'verde_oscuro') & (df_state.State_8h == 'verde_claro')
        
        df_state['General_state'] = np.where(c1 | c2 | c3 | c4 | c5 |c6 , 1, 0)

        return df_state


    def get_more_candles_data_online(self, n_days, time_frame_in_hours):
        """ get the candles data needed to process the strategy """

        n_candles = int(n_days * (24 / time_frame_in_hours))
        #n_candles = self.get_candles_needed_for_running()
        candles = self.prices_repository.get_candles(
            self.base_currency, 
            self.quote_currency, 
            MINUTES_TO_BINANCE_INTERVAL[time_frame_in_hours*60], 
            n_candles)
        return candles
        
        
    def get_candles_needed_for_running(self):
        """ Returns the numbers of candles needed to process the strategy """
        return self.MAX_NUMBER_OF_CANDLES
    
    def increase_values_to_significant_magnitudes(self, candles_data):
        """
        If quote_currency is BTC, the prices are too small, so we need to multiplier for 1e3 in order to avoid 
        numerical errors. 
        """
        if self.quote_currency == 'BTC':
            numeric_columns = candles_data._get_numeric_data().columns
            candles_data[numeric_columns] = 10000*candles_data[numeric_columns]

        return candles_data 

    def run_over_a_df(self, candles): 
        df = candles.copy() 
        df = self.process_candles(df)
        df['signal'] = np.nan
        df = self.check_conditions_to_close_a_position(df) 
        df = self.check_conditions_to_open_a_position(df)
        ##    
        df['signal'] = df['signal'].fillna(method='ffill')
        
        ## Para los primeros nan
        df['signal'] = df['signal'].replace(np.nan,0) 

        df.index = pd.DatetimeIndex([i.replace(tzinfo=timezone.utc) for i in df.index])
        return df

    def get_more_candles_data(self, time_frame_in_hours, start_date, end_date):
        """ get the candles data needed to process the strategy """

        ticker = self.base_currency+'/'+self.quote_currency 
        candles_repo = CandlestickRepository.default_repository()
        candles = candles_repo.get_candlestick(ticker,  'binance', int(time_frame_in_hours*60), start_date, end_date)
        candles = candles.dropna()
        candles = simulations.FillNa().fill_ohlc(candles)

        return candles