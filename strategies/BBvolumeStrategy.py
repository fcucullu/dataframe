from strategies.Holding import Holding
from ta.volatility import keltner_channel_lband,keltner_channel_hband
import numpy as np

class BBvolumeStrategy(Holding): 
    STANDARD_BOUNDS = {'bb_periods': [1,20],
                       'bb_mult_factor': [1,5],
                       'vol_periods': [1,20],
                       'vol_mult_factor_down': [1,5],
                       'vol_mult_factor_up': [1,5]}
    
    def process_candles(self, bb_periods, bb_mult_factor, vol_periods, vol_mult_factor_down, vol_mult_factor_up):
        candles = self.candles        
        bb_periods = int(bb_periods)
        vol_periods = int(vol_periods) 
                
        candles["volume"] = candles["volume"].shift(1)
        candles["high"] = candles["high"].shift(1)
        candles["low"] = candles["low"].shift(1)
        candles["sma"] = candles["open"].rolling(bb_periods).mean()
        candles["std"] = candles["open"].rolling(bb_periods).std()
        candles["bbu"] = candles["sma"] + bb_mult_factor*candles["std"]
        candles["bbl"] = candles["sma"] - bb_mult_factor*candles["std"]
        candles["kcu"] = keltner_channel_hband(high=candles["high"],
                                               low=candles["low"],
                                               close=candles["open"],
                                               window=bb_periods)
        candles["kcl"] = keltner_channel_lband(high=candles["high"],
                                               low=candles["low"],
                                               close=candles["open"],
                                               window=bb_periods)
        candles["vol_change"] = candles["volume"].rolling(vol_periods).mean()
        
        candles["signal"] = np.nan
        candles["signal"] = np.where((candles["bbu"]<candles["close"]) & \
             (candles["kcu"]<candles["close"]) & \
             ((candles["bbu"]>candles["close"].shift(1)) |\
             (candles["kcu"]>candles["close"].shift(1))) &\
             (candles["volume"]>vol_mult_factor_up*candles["vol_change"])\
             , 2,candles["signal"])
        candles["signal"] = np.where((candles["bbl"]>candles["close"]) & \
             (candles["kcl"]>candles["close"]) & \
             ((candles["bbl"]<candles["close"].shift(1)) |\
             (candles["kcl"]<candles["close"].shift(1))) &\
             (candles["volume"]<vol_mult_factor_down*candles["vol_change"])\
             ,1, candles["signal"])
        candles["signal"] = np.where((candles["bbu"]<candles["close"]) &\
             (candles["kcu"]<candles["close"]) &\
             ((candles["bbu"]>candles["close"].shift(1)) |\
             (candles["kcu"]>candles["close"].shift(1))) &\
             (candles["volume"]<vol_mult_factor_up*candles["vol_change"])\
             ,-1, candles["signal"])
        candles["signal"] = np.where((candles["bbl"]>candles["close"]) &\
             (candles["kcl"]>candles["close"]) &\
             ((candles["bbl"]<candles["close"].shift(1)) |\
             (candles["kcl"]<candles["close"].shift(1))) &\
             (candles["volume"]>vol_mult_factor_down*candles["vol_change"])\
             , -2, candles["signal"])
       
        status = candles["signal"].ffill()
        candles["signal"] = np.where((status==2) &\
             (candles["sma"]>=candles["close"])
             ,0, candles["signal"])
        candles["signal"] = np.where((status==-2) &\
             (candles["sma"]<=candles["close"])
             ,0, candles["signal"])
         
        candles["signal"]=candles["signal"].replace(2,1).replace(-2,-1)
        candles["signal"]=candles["signal"].shift(1).ffill().replace(np.nan,0)
        return candles