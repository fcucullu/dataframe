from strategies.Holding import Holding
import numpy as np

class ThreeStandardMovingAverageAlternative(Holding):
    STANDARD_BOUNDS = {'ma_short': [1,30],
                       'ma_medium': [25,60],
                       'ma_long': [51,100]}
    
    def process_candles(self, ma_short, ma_medium, ma_long):
        ma_short = int(ma_short)
        ma_medium = int(ma_medium)
        ma_long = int(ma_long)
        
        candles = self.candles
        candles['short'] = candles.open.rolling(ma_short).mean()
        candles['medium'] = candles.open.rolling(ma_medium).mean()
        candles['long'] = candles.open.rolling(ma_long).mean()
        
        candles['signal'] = 0
        candles['signal'] = np.where((candles.short>=candles.medium) & (candles.medium>=candles.long), 1, candles.signal)
        candles['signal'] = np.where((candles.short<=candles.medium) & (candles.medium<=candles.long), -1, candles.signal)
        
        return candles