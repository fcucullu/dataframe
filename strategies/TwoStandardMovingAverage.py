from strategies.Holding import Holding
import numpy as np

class TwoStandardMovingAverage(Holding):    
    STANDARD_BOUNDS = {'ma_short': [1,50], 'ma_long': [51,100]}
    
    def process_candles(self, ma_short, ma_long):
        ma_short = int(ma_short)
        ma_long = int(ma_long)
        candles = self.candles        
        open_prices = candles.open 
        
        candles['short'] = open_prices.rolling(ma_short).mean()
        candles['long'] = open_prices.rolling(ma_long).mean()
        candles['signal'] = np.where(candles.short >= candles.long, 1, -1)
        return candles        
