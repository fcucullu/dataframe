from strategies.Holding import Holding
import numpy as np

class ThreeStandardMovingAverage(Holding):
    STANDARD_BOUNDS = {'ma_short_buy': [1,50],
                       'ma_short_sell': [5,50],
                       'ma_long': [51,100]}
    
    def process_candles(self, ma_short_buy, ma_short_sell, ma_long):
        ma_short_buy = int(ma_short_buy)
        ma_short_sell = int(ma_short_sell)
        ma_long = int(ma_long)
        candles = self.candles        
        open_prices = candles.open 

        candles['short_buy'] = open_prices.rolling(ma_short_buy).mean()
        candles['short_sell'] = open_prices.rolling(ma_short_sell).mean()
        candles['long'] = open_prices.rolling(ma_long).mean()
        
        candles['signal'] = 0
        candles['signal'] = np.where(candles.short_buy>candles.long, 1, candles.signal)
        candles['signal'] = np.where(candles.short_sell<candles.long, -1, candles.signal)
        return candles