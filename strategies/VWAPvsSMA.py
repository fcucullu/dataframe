from strategies.Holding import Holding
import numpy as np

class VWAPvsSMA(Holding):
    STANDARD_BOUNDS = {'n_candles': [1,10],
                       'min_percentage': [0,0.1]}
    
    def calculate_vwap(self, n_candles):
        candles = self.candles
        volume_times_price_mean = (candles.volume * candles.open).mean()
        volume_mean = candles.volume.shift().rolling(n_candles).mean()
        return volume_times_price_mean / volume_mean
    
    def process_candles(self, n_candles, min_percentage):
        candles = self.candles
        n_candles = int(n_candles) 
        vwap = self.calculate_vwap(n_candles)
        sma = candles.open.rolling(n_candles).mean()
        current_percentage = abs(np.log(vwap/sma))

        candles['signal'] = 0
        candles['signal'] = np.where((current_percentage >= min_percentage) & (vwap > sma), 1, candles["signal"])
        candles['signal'] = np.where((current_percentage >= min_percentage) & (vwap < sma), -1, candles["signal"])
        
        return candles