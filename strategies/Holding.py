from utils.xmetrics import Xmetrics
Xmetrics = Xmetrics()

class Holding:
    def __init__(self, candles):
        self.candles = candles
        self.periods = (self.candles.index[1]-self.candles.index[0]).seconds // 3600

    def process_candles(self, **kwargs):
        candles = self.candles        
        candles['signal'] = 1
        return candles        

    def get_performance(self, **kwargs):
        self.candles = self.process_candles(**kwargs)
        self.candles = Xmetrics.calculate_returns(self.candles)
        self.candles['performance'] = (self.candles['returns']+1).cumprod()
        return self.candles.performance[-1]
    
    def get_standard_bounds(self):
        return self.STANDARD_BOUNDS


class OutOfMarket(Holding):
    def process_candles(self):
        candles = self.candles        
        candles['signal'] = 0
        return candles        