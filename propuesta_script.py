import tesis_utils as utils

from datetime import datetime
import pandas as pd
import numpy as np


#Inputs
start_date = datetime(2021,1,1)
end_date = datetime(2022,1,1)
candles_size_in_minutes = 60*24
exchange = 'binance'
pair = 'BTC/USDT'
CandlestickRepository = utils.CandlestickRepository.default_repository()
Graphs = utils.Graphs()

prices = CandlestickRepository.get_candlestick(pair,
                                               exchange, 
                                               candles_size_in_minutes,
                                               start_date,
                                               end_date)
returns = prices['open'].pct_change()
#por definicion 2:
logreturns = np.log(prices.close / prices.close.shift(1))


###########################################################################
'''                     Grafico N°1                 ''' 

logreturns_affected = logreturns.copy()
loss_efficiency = 0.005
for i in range(0,len(logreturns),5):
    logreturns_affected[i] -= loss_efficiency
performance = logreturns.cumsum()+1
performance_affected = logreturns_affected.cumsum()+1


label_1 = 'Actuación potencial'
label_2 = 'Actuación realizada'

Graphs.graph_comparison_cdf_simulations(performance,label_1,performance_affected,label_2)

